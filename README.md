This is the repository for MATH205 / MATH321 notes.

You'll need to compile notes.tex to get a pdf.

Please use the issue tracker if you have issues. Also feel free to email me 
directly or to approach me in class.
