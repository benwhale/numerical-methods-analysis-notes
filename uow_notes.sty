\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{booktabs}

\usepackage{tensind}
\tensordelimiter{?}
\tensorformat{lrbs}

%% minted stuff
\usepackage[newfloat]{minted}
\usemintedstyle{friendly}

% The code for this listing environment is a combination of information
% from the follow links and reading the documentation for the minted and caption
% packages
%
% https://tex.stackexchange.com/questions/19579/horizontal-line-spanning-the-entire-document-in-latex
% https://tex.stackexchange.com/questions/254044/caption-and-label-on-minted-code
% https://stackoverflow.com/questions/741985/latex-source-code-listing-like-in-professional-books
\usepackage{caption}
\SetupFloatingEnvironment{listing}{name=Listing, listname=Code examples}
\DeclareCaptionFormat{listing}{{\parbox{\textwidth}{%
\rule{\textwidth}{0.5pt}
{#1#2#3}%
\vspace{-5.5ex}}}}
\captionsetup[listing]{font=footnotesize,format=listing}

\newenvironment{longlisting}{\captionsetup{type=listing}}{}

\usepackage{scrhack}

\setminted[python]{
  linenos=true,
  autogobble,
  fontseries=\ttdefault,
  fontfamily=tt,
  escapeinside=||,
  mathescape,
  fontsize=\footnotesize,
  frame=lines
}
\newenvironment{python}
{\VerbatimEnvironment\begin{minted}{python}} 
{\end{minted}}
\newmintinline{python}{}

\setminted[c]{
  linenos=true,
  autogobble,
  fontseries=\ttdefault,
  fontfamily=tt,
  escapeinside=||,
  mathescape,
  fontsize=\footnotesize,
  frame=lines
}
\newenvironment{cmint}
{\VerbatimEnvironment\begin{minted}{c}} 
{\end{minted}}
\newmintinline{c}{}

\setminted[bash]{
  autogobble,
  escapeinside=||,
  fontseries=\ttdefault,
  fontfamily=tt,
  mathescape,
  fontsize=\footnotesize,
  frame=lines
}
\newenvironment{bash}
{\VerbatimEnvironment\begin{minted}{bash}} 
{\end{minted}}
\newmintinline{bash}{}

\setminted[latex]{
  linenos=true,
  autogobble,
  fontseries=\ttdefault,
  fontfamily=tt,
  escapeinside=||,
  mathescape,
  fontsize=\footnotesize,
  frame=lines
}
\newenvironment{latex}
{\VerbatimEnvironment\begin{minted}{latex}} 
{\end{latex}}
\newmintinline{latex}{}




% Bibliography
\usepackage[sort,comma,authoryear]{natbib}
\bibliographystyle{abbrvnat}

% Theorems
\newtheorem{theorem}{Theorem}[section]
\newtheorem{definition}[theorem]{Definition}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{examplex}[theorem]{Example}
\newtheorem{exercisex}[theorem]{Exercise}

% see
% https://tex.stackexchange.com/questions/16453/denoting-the-end-of-example-remark
% for where I got this from
%
% puts a triangle in instead of a qed symbol like in a proof environment
\newenvironment{example}
{\pushQED{\qed}\renewcommand{\qedsymbol}{$\triangle$}\examplex}
{\popQED\endexamplex}

\newenvironment{exercise}
{\pushQED{\qed}\renewcommand{\qedsymbol}{$\triangle$}\exercisex}
{\popQED\endexercisex}

% a proof-like "answer" environment
\makeatletter
\newenvironment{answer}[1][\solutionname]{\par
  \pushQED{\qed}%
  \normalfont \topsep6\p@\@plus6\p@\relax
  \trivlist
  \item[\hskip\labelsep
\itshape
    #1\@addpunct{.}]\ignorespaces
}{%
  \popQED\endtrivlist\@endpefalse
}
\providecommand{\solutionname}{Answer}
\makeatother

% Load hyperref last
% https://tex.stackexchange.com/questions/413843/pdftex-warning-destination-with-the-same-identifier-namepage-i-has-been-alr
\usepackage{hyperref}

% Attibutation quotes
\def\signed #1{{\leavevmode\unskip\nobreak\hfil\penalty50\hskip2em
  \hbox{}\nobreak\hfil(#1)%
  \parfillskip=0pt \finalhyphendemerits=0 \endgraf}}

\newsavebox\mybox
\newenvironment{aquote}[1]
  {\savebox\mybox{#1}\begin{quote}}
  {\signed{\usebox\mybox}\end{quote}}

% Commands for these notes
\newcommand{\fpvalue}[2]{\ensuremath{{#2}\times b^{{#2}-q}}}
\newcommand{\fp}[1]{\operatorname{fl}\left({#1}\right)}
\newcommand{\fpadd}{\ensuremath{\oplus}}
\newcommand{\fpsub}{\ensuremath{\ominus}}
\newcommand{\fptim}{\ensuremath{\otimes}}
\newcommand{\fpdiv}{\ensuremath{\oslash}}
\newcommand{\round}[1]{\operatorname{round}\left({#1}\right)}
\newcommand{\Cond}[1]{\operatorname{Cond}\left({#1}\right)}
\newcommand{\cond}[1]{\operatorname{cond}\left({#1}\right)}

% Math commands
\newcommand{\dd}{\operatorname{d}\hspace{-0.4ex}}
\newcommand{\grad}{\ensuremath{\textrm{grad}}}
\newcommand{\diag}{\operatorname{diag}}
\newcommand{\abs}[1]{\ensuremath{\left\lvert{#1}\right\rvert}}
\newcommand{\norm}[1]{\ensuremath{\left\lVert{#1}\right\rVert}}
\newcommand{\ran}{\ensuremath{\textrm{ran}}}
\newcommand{\dom}{\ensuremath{\textrm{dom}}}
\newcommand{\R}{\ensuremath{\mathbb{R}}}
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\tr}[1]{\operatorname{tr}\left({#1}\right)}
\newcommand{\trace}{\operatorname{trace}}
\newcommand{\floor}[1]{\operatorname{floor}\left\lfloor{#1}\right\rfloor}
\newcommand{\sign}[1]{\operatorname{sign}\left\lfloor{#1}\right\rfloor}
\newcommand{\SPAN}[1]{\operatorname{span}\left({#1}\right)}

% Personally comments
\newcommand{\ben}[1]{\footnote{\textcolor{red}{#1}}}
