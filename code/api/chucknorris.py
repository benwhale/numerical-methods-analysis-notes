import json
import requests

get_random_joke = "https://api.chucknorris.io/jokes/random"

r = requests.get(get_random_joke)
data = json.loads(r.text)
print json.dumps(data, indent=4, sort_keys=True)
