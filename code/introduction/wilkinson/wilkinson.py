import numpy as np
import matplotlib.pyplot as plt

def w_expanded(x):
    return x**20 - 210*x**19 + 20615*x**18 - 1256850*x**17 + 53327946*x**16 - 1672280820*x**15 + 40171771630*x**14 - 756111184500*x**13 + 11310276995381*x**12 - 135585182899530*x**11 + 1307535010540395*x**10 - 10142299865511450*x**9 + 63030812099294896*x**8 - 311333643161390640*x**7 + 1206647803780373360*x**6 - 3599979517947607200*x**5 + 8037811822645051776*x**4 - 12870931245150988800*x**3 + 13803759753640704000*x**2 - 8752948036761600000*x + 2432902008176640000.  

def w_expanded_derivative(x):
    return 20*x**19 - 210*19*x**18 + 20615*18*x**17 - 1256850*17*x**16 + 53327946*16*x**15 - 1672280820*15*x**14 + 40171771630*14*x**13 - 756111184500*13*x**12 + 11310276995381*12*x**11 - 135585182899530*11*x**10 + 1307535010540395*10*x**9 - 10142299865511450*9*x**8 + 63030812099294896*8*x**7 - 311333643161390640*7*x**6 + 1206647803780373360*6*x**5 - 3599979517947607200*5*x**4 + 8037811822645051776*4*x**3 - 12870931245150988800*3*x**2 + 13803759753640704000*2*x - 8752948036761600000 

def w_factorised(x):
    value = 1
    for i in range(1, 21):
        value = value * (x-i)
    return value

def w_factorised_derivative(x):
    sums = 0
    for j in range(1, 21):
        value = 1
        for i in range(1, 21):
            if i == j:
                continue
            value = value * (x-i)
        sums = sums + value
    return sums

def compare_functions(function_1, function_2, x_range):
    values_1 = []
    values_2 = []
    for x in x_range:
        values_1 += [function_1(x)]
        values_2 += [function_2(x)]
    plt.plot(x_range, values_1, x_range, values_2)
    plt.show()

#compare_functions(w_expanded_derivative, w_factorised_derivative, np.linspace(-1,21,200))

def newton(function, derivative, value):

    def newton_iterate(function, derivative, value):
        return value - function(value) / derivative(value)

    print "Staring value = " + str(value)
    old_value = value
    for i in range(10):
        value = newton_iterate(function, derivative, value)
        print "New value = " + str(value),
        print "    Absolute error = " + str(abs(old_value - value))
        old_value = value

#newton(w_expanded, w_expanded_derivative, 0.5)
#newton(w_factorised, w_factorised_derivative, 0.5)
newton(w_expanded, w_expanded_derivative, 15.1)
#newton(w_factorised, w_factorised_derivative, 15.1)



