import sys

def logistic_map(x, r):
    return r*x*(1-x)

def iterate(start_value, r, number_of_iterates):
    print "Starting value is = " + str(start_value)
    value = start_value
    for i in range(number_of_iterates):
        new_value = logistic_map(value, r)
        print "New value = " + str(new_value),
        print "    relative error is = " + str(abs((new_value - value)/value))
        value = new_value

iterate(float(sys.argv[1]), float(sys.argv[2]), int(sys.argv[3]))
