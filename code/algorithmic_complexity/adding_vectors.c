#include <math.h>
#include <stdio.h>

double add_vectors(double *first, int first_length, double *second, int second_length) {
  double sum = 0;                           
  for(int i = 0; i < first_length; i++) {   
    sum += first[i];
  }
  for(int i = 0; i < second_length; i++) {
    sum += second[i];
  }
  return sum;

}

int main() {
  int first_array_size = 100;
  double first[first_array_size];
  for(int i = 0; i < first_array_size; i++) {
    first[i] = i;
  }

  int second_array_size = 100;
  double second[second_array_size];
  for(int i = 0; i < second_array_size; i++) {
    second[i] = i*i;
  }
  
  double sum = add_vectors(first, first_array_size, second, second_array_size);
  printf("%f", sum);
  return 0;
}
