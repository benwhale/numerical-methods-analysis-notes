#include <stdio.h>

int main() {
  double a = 0.1;
  double b = 0.1;
  printf("0.1 - 0.1 = %.55f\n", a - b);
  printf("0.1 = %.55f\n\n", b);

  a = 0.3;
  printf("0.3 - 0.1 = %.55f\n", a - b);
  double c = 0.2;
  printf("0.2 = %.55f\n\n", c);

  a = 0.2;
  printf("0.2 + 0.1 = %.55f\n", a + b);
  c = 0.3;
  printf("0.3 = %.55f\n\n", c);
}
