import sys

def fixed_point(r):
    return (r-1)/r

r = float(sys.argv[1])
x = float(sys.argv[2])
for i in range(10000):
    x = r * x * (1-x)

for i in range(10):
    x = r * x * (1-x)
    print x
