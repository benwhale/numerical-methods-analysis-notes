%colors linux
var("x r")
f = function('f')(x)
f(x) = r * x * (1-x)
solutions = solve([f(f(x))==x],x)
solutions
g = function('g')(x)
g = f(f(x))-x
h = function('h')(x)
h = f(x)-x
g.maxima_methods().divide(h)
solve([g.maxima_methods().divide(h)[0]==0],x)
solutions = solve([g.maxima_methods().divide(h)[0]==0],x)
solutions[0].subs(r=3.3)
solutions[1].subs(r=3.3)
%history -f logistic_sage_example.py
