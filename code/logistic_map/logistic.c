#include <stdio.h>

double fixed_value(double r){
  return (r-1)/r;
}

int main(){
  for(int i=0; i<10; i++) {
    double r = 0 + 0.4*i;
    printf("r = %.55f\n", fixed_value(r));
  }
}

