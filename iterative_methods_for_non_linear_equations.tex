\chapter{Iterative methods for non linear equations}

  This chapter is concerned with
  finding values $x\in\R^n$ so that
  if $f:\R^n\to\R^m$ is a given function,
  then $f(x)=0$.

  \begin{definition}
    Let $f:\R^n\to\R^m$. If $x\in\R^n$ is such
    that $f(x)+0$ then $x$ is a root of $f$.
  \end{definition}

  Before attempting to compute such values it is important to know that roots
  exist.

  \begin{example}
    What happens if Newtonian iteration is applied
    to $f(x)=x^2+1$?
  \end{example}

  \begin{theorem}[{\cite[Theorem 1.1]{suli2003introduction}}]
    \label{thm_neg_prod_implies_root}
    If $f:\R\to\R$ continuous on $[a,b]$
    is such that $f(a)f(b)\leq 0$ then there
    exists $x\in[a,b]$ so that $f(x)=0$.
  \end{theorem}
  \begin{proof}
    Exercise. You will need to use the intermediate value theorem,
    Theorem \ref{thm_intermediate_value_theorem}.
  \end{proof}

  \begin{definition}
    Let $f:\R\to\R$ be continuous on $[a,b]$ if
    $f(a)f(b)\leq 0$ then we say that $[a,b]$ brackets a root (or zero)
    of $f$.
  \end{definition}

  There is a large variety of ``bracketing'' root finding methods. They
  tend to provide superior error estimates relative to non-bracketing
  root finding methods. This is because a bracketing method conclusively tells
  you where the root is, even if that location is within a large interval.
  Non-bracketing methods only give an estimated value for the root.
  Determination of whether or not the estimate is good can be hard.

  \section{Bisection method}

    Theorem \ref{thm_neg_prod_implies_root} suggests an easy algorithm
    for finding roots. Listing \ref{listing_python_bisect}.
    \begin{definition}
      Let $f:\R\to\R$ be continuous on $[a,b]$ so that
      $f(a)f(b)\leq 0$. 
      The bisection method is the following algorithm for generating
      a sequence of intervals.
      Let $a_0=a$ and $b_0=b$. If $f(a_0)=0$ or
      $f(b_0)=0$ then we have found
      a root. Stop the algorithm. Otherwise, suppose
      that for $i\in N$, $a_i,b_i\in\R$ are such that
      $a_i\neq b_i$, $f(a_i)f(b_i)\leq 0$.
      If $f(a_i)=0$ or $f(b_i)=0$ we have found a root.
      Stop the algorithm. Otherwise let $c=\frac{1}{2}(a_i+b_i)$.
      If $f(a_i)f(c)\leq 0$ then let $a_{i+1}=a_i$ and $b_{i+1}=c$,
      otherwise let $a_{i+1}=c$ and $b_{i+1}=b_i$.
    \end{definition}
    \begin{listing}
      \caption{Python code for the bisection method}
      \label{listing_python_bisect}
      \begin{python}
        def bisection(function, a, b, tollerance):
            
            def print_data(a_value, b_value, a, b, error):
                print "Interval = [" + str(a) + ", " + str(b) + "]"
                print "  Values = [" + str(a_value) + ", " + str(b_value) + "]"
                print "  error = " + str(error)
            
            def equal_up_to_tollerance(y, x, tollerance):
                return abs(y-x) < tollerance

            def bisection_iterate(function, a, b, tollerance):

                a_value = function(a)
                if equal_up_to_tollerance(a_value, 0, tollerance):
                    print "Root is = " + str(a)
                    return a_value, None

                b_value = function(b)
                if equal_up_to_tollerance(b_value, 0, tollerance):
                    print "Root is = " + str(b)
                    return b_value, None

                mid = 0.5 * (a + b)
                mid_value = function(mid)
                if equal_up_to_tollerance(mid_value, 0, tollerance):
                    print "Root is " + str(mid)
                    return mid, None

                product = a_value * b_value
                if product > 0:
                    raise Exception("No root in interval")

                print_data(a_value, b_value, a, b, b-a)
                if a_value * mid_value < 0:
                    return (a, mid)
                return (mid, b)

            a, b = bisection_iterate(function, a, b, tollerance)
            while(b is not None and not equal_up_to_tollerance(a, b, tollerance)):
                a, b = bisection_iterate(function, a, b, tollerance)
      \end{python}
    \end{listing}

    \begin{lemma}
      \label{lem_bisection_properties}
      Let $f:\R\to\R$ be continuous on $[a,b]$. If
      $([a_i,b_i])_{i\in\N}$ is a 
      sequence of intervals that results
      from applying the bisection method, then
      \begin{itemize}
        \item $\displaystyle \abs{a_{i+1}-b_{i+1}}=\frac{1}{2}\abs{a_{i}-b_{i}}$,
        \item the sequences $(a_i)_{i\in\N}$ and $(b_i)_{i\in\N}$ are Cauchy,
        \item there exists $\alpha\in[a,b]$ so that
          $a_i\to \alpha$ and $b_i\to\alpha$ and $f(\alpha)=0$.
      \end{itemize}
    \end{lemma}
    \begin{proof}
      Exercise.
    \end{proof}

    \begin{lemma}
      Any sequence of iterates produced by the bisection method has
      linear convergence.
    \end{lemma}
    \begin{proof}
      Exercise.
    \end{proof}

    If the initial interval contains multiple roots then which root is
    returned is the result of subtle interaction between the code and
    the initial interval.

    \begin{exercise}
      Let $f:\R\to\R$ be defined by $f(x)=x(x+1)(x-1)$.
      What is the result of applying the bisection method, as described in
      Listing \ref{listing_python_bisect}, to the following intervals
      $[-2,2]$, $[-1,1]$, $[-2, 4]$, $[-5, 4]$. What is the result
      if lines 13--16 are swapped with lines 18--21? What if
      lines 34--36 were
      {
        \begin{minted}[firstnumber=34]{python}
          if b_value * mid_value < 0:
            return (mid, b)
          return (a, mid)
        \end{minted}
      }
      instead?
    \end{exercise}

    \begin{exercise}
      What is the minimum interval length in a floating point system? What
      does this imply about when the computation of iterates for the bisection
      method should be stopped? What is the maximum number of iterates
      to be computed on a 64 bit machine?
      Should you stop earlier to allow for 
      the accumulation of round off error?
    \end{exercise}

    \begin{exercise}
      Using the standard model of floating point arithmetic write down
      a formula for $a_i$ and $b_i$ in terms of $a_{0}$ and $b_0$.
      Since we are only considering error here you can assume that
      for all $i\in\N$
      \[
        f(a_i)f\left(\frac{a_i + b_i}{2}\right)<0.
      \]
      What does this formula imply about when you should stop computation
      of bisection iterates.
    \end{exercise}

    \begin{exercise}
      Lemma \ref{lem_bisection_properties} implies that the bisection
      method will always converge. Let $w:\R\to\R$ be the degree $60$
      variant of Wilkinson's polynomial. Prove that $w(14.9)w(15.1)<0$.
      Apply bisection method to 
      $[14.9,15.1]$. What goes wrong and why?
    \end{exercise}

    \begin{proposition}
      \label{lem_bisection_convergence}
      Let $f:\R\to\R$ be continuous on $[a,b]$. Let
      $([a_i,b_i])_{i\in\N}$ be a 
      sequence of intervals that results
      from applying the bisection method.
      If for all $i\in\N$, $\epsilon_i=\abs{b_i-a_i}$ then
      the sequence $(\epsilon_i)_{i\in\N}$ converges linearly.
    \end{proposition}
    \begin{proof}
      Exercise. This is a consequence of Lemma \ref{lem_bisection_properties}.
    \end{proof}
    
  \section{False position}

    It is possible to generalise the bisection method.

    \begin{definition}
      Let $m\in\N$ and
      Let $g:\R^m\to\R$. Let $x=(x_0,x_1,\ldots,x_{m-1})\in\R^m$
      the iterates (sometimes complex iterates - no relation to
      complex numbers) of $g$ from $x$
      is the sequence of points $(x_i)_{i\in\N}$ defined by
      \[
        x_{i+1}=g(x_i,x_{i-1},\ldots, x_{i-m}).
      \]
    \end{definition}

    \begin{lemma}
      Given two points $(x_0,y_0)$ and $(x_1,y_1$ the line between
      them is 
      \begin{align*}
        y&=y_0\frac{x-x_1}{x_0-x_1} + y_1\frac{x-x_0}{x_1-x_0}\\
          &=\frac{1}{x_0-x_1}((y_0-y_1)x +y_1x_0-y_0x_1,\\
          &= \frac{y_1 - y_0}{x_1-x_0}(x-x_1) + y_1.
      \end{align*}
      This line crosses the $x$-axis at
      \begin{align*}
        x = x_1 + y_1\frac{x_1-x_0}{y_1-y_0}
         = \frac{y_0x_1 - y_1x_0}{y_0-y_1}.
      \end{align*}
    \end{lemma}
    \begin{proof}
      Exercise. Almost the easiest thing in the subject.
    \end{proof}

    \begin{definition}
      Let $f:\R\to\R$ and define $g:\R^2\setminus\{(x,x):x\in\R\}\to R$ by
      \[
        g(x,y)=\frac{f(y)x - f(x)y}{y-x}
      \]
      The false position method is the algorithm for the generation of
      a sequence of intervals. The algorithm is the same as for the bisection
      method except that rather than using the mid point of the interval
      the result of $g(a_i, b_i)$ is used.
    \end{definition}

    The geometric interpretation of the false position method is that,
    rather than using the mid point, a line is drawn between the two known
    data points $(a_i,f(a_i)$, $(b_i, f(b_i))$ and the intersection of
    this line with the $x$-axis is used. Since $f(a_i)$ and $f(b_i)$ are
    computed in the bisection method anyway the method of false position
    can offer an computational advantage.

    \begin{lemma}
      \label{lem_false_position_basic_properties}
      Let $f:\R\to\R$ be continuous on $[a,b]$. If
      $([a_i,b_i])_{i\in\N}$ is a 
      sequence of intervals that results
      from applying the method of false position, then
      \begin{itemize}
        \item the sequences $(a_i)_{i\in\N}$ and $(b_i)_{i\in\N}$ are Cauchy,
        \item there exists $\alpha\in[a,b]$ so that 
          $f(\alpha)=0$ and
          either
          $a_i\to \alpha$ or $b_i\to\alpha$ or both.
      \end{itemize}
    \end{lemma}
    \begin{proof}
      Exercise.
    \end{proof}

    At this point you should compare Lemma 
    \ref{lem_false_position_basic_properties} to Lemma
    \ref{lem_bisection_properties}. We have lost information about the
    change in 
    lengths of intervals and we are no longer guaranteed to have both end points
    converging to the root.

    \begin{definition}
      Let $f:\R\to\R$ be continuous on $[a,b]$ and let
      $([a_i,b_i])_{i\in\N}$ be a 
      sequence of intervals that results
      from applying the method of false position.
      If, for all $i\in\N$, $a_i=a$ or $b_i=b$ \emph{but not both},
      then we say that the method of false position has one sided convergence.
    \end{definition}

    I claim, but will not prove,  that the method
    of false position converges faster that the bisection method if
    one sided convergence is avoided. If one sided convergence occurs
    then the method of false position may or may not converge faster than
    the bisection method. %There are examples where it is slower.

    \begin{exercise}
      Find a function for which the method of false position has one
      sided convergence and the convergence rate of the sequence
      $\abs{b_i-a_i}$ is slower than the bisection method applied
      to the same function with the same initial interval.
    \end{exercise}

    \begin{exercise}
      Perform a rounding error analysis to justify the use of the formula
      \[
        \frac{y_0x_1 - y_1x_0}{y_0-y_1}
      \]
      over
      \[
        x = x_1 + y_1\frac{x_1-x_0}{y_1-y_0}
      \]
      in the method of false position. To do this you will need
      to consider the signs of $x_0,x_1$ and the size of $\abs{x_1-x_0}$.
    \end{exercise}

    Determining the convergence rate of the sequence of iterates
    produced by the method of false position is not easy.

    \begin{exercise}
      Let $f_1, f_2:\R\to\R$ be defined by $f_1(x)=x(x-1)(x+1)$ and 
      $f_2(x)=x+1$.
      Compare the numerical convergence rates of the bisection method and 
      the method of false position over the interval $[0.1, 1.2]$.
      Find a formula for a function so that over a specified interval
      the method of false position has sub-linear convergence.
    \end{exercise}

    It is possible to further generalise this. For the $i$'th iterate we
    have $2(i-1)$ points $(a_j, f(a_j)),(b_j, f(b_j))$, 
    $j=0,\ldots, i-1$. We can therefore
    estimate the function the function with a $2(i-1)-1$ degree polynomial.
    These higher order versions of the bisection and false position methods
    arn't always worth the additional computations. It depends a bit of
    the type of application.


  \section{Some theory about fixed points}

    It is sometimes difficult to check the condition in Theorem
    \ref{thm_neg_prod_implies_root}.

    \begin{example}[{\cite[Page 3]{suli2003introduction}}]
      Let
      \[
        f(x)= \frac{1}{2} + \frac{1}{1 + M\abs{x-1.05}}.
      \]
      Algebraically determine the roots of $f$. Give
      formula for the intervals, $[a,b]$, around each root so that
      $f(a)f(b)\leq 0$.

      Make plots of the function for $M=200, 2,000, 20,000, 200,000, 2,000,000$
      and $20,000,000$ from $0.8$ to $1.8$ and $1.04999$ to $1.05001$.
    \end{example}

    Another way to test if there is a root is the following.

    \begin{theorem}[{\cite[Theorem 1.2]{suli2003introduction}}]
      \label{thm_1d_brower_fixed_point}
      If $g:\R\to\R$ be continuous on $[a,b]$ and such that
      $g([a,b])\subset [a,b]$, then there exists $x\in[a,b]$ 
      so that $g(x)=x$.
    \end{theorem}
    \begin{proof}
      Exercise.
      This follows from Theorem \ref{thm_neg_prod_implies_root}.
    \end{proof}

    \begin{definition}
      Let $g:\R\to\R$. If $x\in \R$ is such that $g(x)=x$ then
      $x$ is a fixed point for $g$.
    \end{definition}

    \begin{lemma}
      \label{lem_roots_and_fixed_points}
      Let $f:\R\to\R$ and define $g:\R\to\R$
      by $g(x)=f(x)-x$. Show that $g(x)=x$ if and only if $f(x)=0$.
    \end{lemma}
    \begin{proof}
      Exercise.
    \end{proof}

    Lemma \ref{lem_roots_and_fixed_points} gives one method for converting
    an equation for a root into an equation for a fixed point.
    This is not a unique method, there are others, and it matters how you
    do it.

    \begin{exercise}[{\cite[Page 5]{suli2003introduction}}]
      Let $f(x)=\exp(x) - 2x - 1$. Suppose that $u\in\R$ is such that
      $f(u)=0$. Show that 
      \[
        u = \ln(2u - 1) = \frac{\exp(u) -1}{2},
      \]
      $u$ is the only root of $f$
      and that $u\in[1,2]$
      Let $g_1(x)\ln(2x-1)$ and 
      \[
        g_2(x)=\frac{\exp(x)-1}{2}.
      \]
      Show that $g_1([1,2])\subset [1,2]$ but that
      $g_2([1,2])\not\subset[1,2]$. Thus when looking for 
      an application of Theorem \ref{thm_1d_brower_fixed_point}
      it matters how we write the fixed point function.
      
      Numerically compute the sequences $x_{i+1}=g_j(x_i)$, $j=1,2$,
      for a range of values in $[1,2]$. What does your results say about
      Theorem \ref{thm_1d_brower_fixed_point}.
    \end{exercise}

    \begin{definition}[{\cite[Definition 1.1]{suli2003introduction}}]
      Let $g:\R\to\R$. Let $x_0\in\R$ and for each
      $i\in\N$ define
      $x_{i+1}=g(x_i)$. The sequence $(x_i)_{i\in\N}$ is called
      the iterates (sometimes the simple iterates) of $g$ from $x_0$.
    \end{definition}

    \begin{lemma}
      Let $g:\R\to\R$ be continuous and let $(x_i)_{i\in\N}$ be the
      iterates of $g$ from some $x_0\in\R$. If there
      exists $\alpha\in\R$ so that $x_i\to\alpha$ then
      $g(\alpha)=\alpha$.
    \end{lemma}
    \begin{proof}
      Exercise. You'll have to use the continuity of $g$.
    \end{proof}

    \subsection{Three core theorems}

      \begin{theorem}[{\cite[Theorem 3.1.2]{isaacson2012analysis}}]
        Let $g:\R\to\R$ be a function
        and suppose that there exists $\alpha\in\R$ so that
        $g(\alpha)=\alpha$.
        If there exists $\rho\in\R$ and $\lambda\in[0,1)$ so that
        for all $x\in[\alpha-\rho, \alpha+\rho]$
        we have 
        \[
          \abs{g(x)-g(\alpha)}\leq\lambda\abs{x-\alpha}
        \]
        then for any $x_0\in[\alpha-\rho,\alpha+\rho]$
        \begin{enumerate}
          \item all iterates $x_{i+1}=g(x_i)$ are in $[x_0-\rho, x_0+\rho]$,
          \item the iterates converge to some point $\alpha$, that is
            \[
              \lim_{i\to\infty}x_i=\alpha
            \]
          \item the point $\alpha$ is the only root in $[x_0-\rho,x_0+\rho]$.
        \end{enumerate}
      \end{theorem}

      \begin{theorem}[{\cite[Theorem 3.1.1]{isaacson2012analysis}}]
        Let $g:\R\to\R$ be a function,
        let $x_0,\rho\in\R$ and let $g$ be Lipschitz on
        $[x_0-\rho, x_0+\rho]$ with constant $\lambda\in[0,1)$.
        If $x_0$ is such that
        \[
          \abs{x_0-g(x_0)}\leq (1-\lambda)\rho
        \]
        then
        \begin{enumerate}
          \item all iterates $x_{i+1}=g(x_i)$ are in $[x_0-\rho, x_0+\rho]$,
          \item the iterates converge to some point $\alpha$, that is
            \[
              \lim_{i\to\infty}x_i=\alpha
            \]
          \item the point $\alpha$ is the only root in $[x_0-\rho,x_0+\rho]$.
        \end{enumerate}
      \end{theorem}

      Of course when performing the computation of $g$ there will be error.
      Thus our $g_A(x)=g(x) + \delta(x)$ where $\delta:\R\to\R$ is a function
      which describes the error. Earlier in the notes I denoted it
      as $\delta y$. Here I want to emphasise that it is a function depending
      on $x$. This is the reason for the change in notation.
      Thus we can write for our iterates
      \[
        x_{i+1}=g(x_i) + \delta(x_i).
      \]
      Analysis of $g$ will often result in an upper bound for $\delta(x)$.
      This occurs when $g$ is backward stable, for example.
      Thus I happy assume that it is normal to find $\delta>0$
      so that for all $i\in\N$, $\abs{\delta(x_i)}<\delta$.
      We need a new theorem to account for this error.

      \begin{theorem}[{\cite[Theorem 3.1.3]{isaacson2012analysis}}]
        Let $g:\R\to\R$ be a function
        and suppose that there exists $\alpha\in\R$ so that
        $g(\alpha)=\alpha$. Let $x_0\in\R$ and let $\delta_{i+1}$ be such that
        \[
          x_{i+1}=g(x_i)+\delta_{i+1}.
        \]
        Suppose that there exists $\delta>0$ so that for all $i\in\N$,
        $\abs{\delta_i}<\delta$.
        If there exists $\rho,\rho_0\in\R$ and $\lambda\in[0,1)$ so that
        for all $x\in[\alpha-\rho, \alpha+\rho]$
        we have 
        \[
          \abs{g(x)-g(\alpha)}\leq\lambda\abs{x-\alpha},
        \]
        and
        \[
          0<\rho_0\leq \rho - \frac{\delta}{1-\lambda},
        \]
        then for any $x_0\in[\alpha-\rho_0,\alpha+\rho_0]$
        all iterates $x_{i+1}$ are in the interval
        $[\alpha - \rho,\alpha+\rho]$ and
        \[
          \abs{\alpha-x_i}\leq \frac{\delta}{1-\lambda} + 
            \lambda^k\left(\rho_0 - \frac{\delta}{1-\lambda}\right).
        \]
      \end{theorem}




  \section{Newton's method}

    \begin{definition}
      Let $f\R\to\R$ be $C^1$ and let
      $g:\R\to\R$ be defined by
      \[
        g(x)=x - \frac{f(x)}{f'(x)}.
      \]
      Newton's method to calculate roots
      of $f$ is the calculation of the iterates of $g$ from $x_0\in\R$ for
      any $x_0$.
    \end{definition}

    \begin{lemma}
      \label{lem_tangent_line_xaxis_intersection}
      Let $f\R\to\R$ be $C^1$. If $u\in\R$ then the line tangent
      to the curve $t\mapsto (t, f(t))$ at $(u,f(u))$ is
      \[
        y = f'(u)(x-u) + f(u).
      \]
      This line intersects the $x$-axis at
      \[
        x = u-\frac{f(u)}{f'(u)}.
      \]
    \end{lemma}
    \begin{proof}
      Exercise.
    \end{proof}

    The point of Lemma \ref{lem_tangent_line_xaxis_intersection} shows that
    the geometric interpretation of Newton's method is that
    we estimate $f$ by its tangent line. Taylor's theorem
    tells us that this will be a reasonable assumption when our iterate
    $x_i$ is close to the root. No guaranties when $x_i$ is far away from
    the root.

    INTERPRETATION OF THEOREMS.

    \begin{exercise}
      Newton's method does not care if there is or is not a root.
      What happens if you apply Newton's method to
      the function defined by $f(x)=x^2 + 1$ starting from $x_0=0.5$.
      What happens if you apply the bisection and false position methods
      on the interval $[-0.5, 0.5]$?
    \end{exercise}

    \begin{exercise}
      Even if there is a root Newton's method is not guaranteed to converge.
      Compare the results of using Newton's method, starting at $x_0=15.1$
      What goes wrong with Newton's method and why does this happen?
    \end{exercise}

    \begin{exercise}
      Even with no numerical error issues Newton's method is not guaranteed to
      converge. Apply Newton's method to the logistic map with
      $r=3.81$. What goes wrong and why?
    \end{exercise}

    \begin{lemma}
      \label{newton_basic_convergence_formula}
      Let $f:\R\to\R$ be $C^1$ and let $f''\in AC$.
      Let $(x_i)_{i\in\N}$ be a sequence of iterates generated by
      Newton's method. If there exists $\alpha\in\R$ so that
      $x_i\to\alpha$ then the sequence $(\epsilon_i)_{i\in\N}$ of errors,
      defined by $\epsilon_i=\alpha-x_i$ for all $i\in\N$,
      is such that
      \[
        \epsilon_{i+1}= -\frac{1}{2}\frac{f''(\theta_i)}{f(x_i)}\epsilon_i^2,
      \]
      as long as $x_i\neq\alpha$. If $x_i=\alpha$ then
      $\epsilon_i=\epsilon_{i+1}=0$.
    \end{lemma}
    \begin{proof}
      Let $i\in\N$. The result is clear if $x_i=\alpha$ so 
      suppose that $x_i<\alpha$. 
      The remaining case,
      $\alpha<x_i$ below, is left as an exercise for you.
      See Exercise \ref{exercise_missing_case_of_newton_basic_convergence_formula}.

      Taylor's theorem, Theorem \ref{thm_taylor_integral_remainder},
      implies that
      \[
        0 = f(\alpha) = f(x_i) + f'(x_i)(\alpha - x_i) + 
          \int_{x_i}^\alpha f''(t)(\alpha-t)\dd t.
      \]
      We know that
      \[
        x_{i+1}=x_i - \frac{f(x_i)}{f'(x_i)}.
      \]
      That is $f'(x_i)(x_{i+1}-x_{i}) + f(x_i)=0$.
      Thus
      \begin{align*}
        0 & = 0 - 0 \\
          & = f(x_i) - f(x_i) + f'(x_i)(\alpha - x_i) - f'(x_i)(x_{i+1}-x_i)
            +
            \int_{x_i}^\alpha f''(t)(\alpha-t)\dd t\\
          & = f'(x_i)(\alpha - x_{i+1})
            +
            \int_{x_i}^\alpha f''(t)(\alpha-t)\dd t.
      \end{align*}
      Since $(\alpha -t)^2\leq 0$ Lemma \ref{lem_yes_you_do_need_to_know_this}
      implies that there exists $\theta_i\in[x_i,\alpha]$ so that
      \begin{align*}
        0 & = f'(x_i)(\alpha - x_{i+1})
            +
            f''(\theta_i)\int_{x_i}^\alpha (\alpha-t)\dd t\\
          & = f'(x_i)(\alpha - x_{i+1})
            +
            f''(\theta_i)\frac{1}{2}(\alpha-x_i)^2\dd t.
      \end{align*}
      A little algebra now gives the result.

      When $\alpha < x_i$ we have
      \[
        f(x_i) = f(\alpha) - f'(x_i)(\alpha - x_i) -
          \int_{x_i}^\alpha f''(t)(\alpha-t)\dd t.
      \]
      Thus
      \begin{align*}
        0 &= -f(x_i) - f'(x_i)(\alpha - x_i) -
          \int_{x_i}^\alpha f''(t)(\alpha-t)\dd t\\
          &= f(x_i)  f'(x_i)(\alpha - x_i) 
          \int_{x_i}^\alpha f''(t)(\alpha-t)\dd t,
      \end{align*}
      and the proof now proceeds as before.
    \end{proof}

    \begin{exercise}
      \label{exercise_missing_case_of_newton_basic_convergence_formula}
      Provide the proof of Lemma \ref{newton_basic_convergence_formula}
      for the case $\alpha<x_i$.
    \end{exercise}
    
    \begin{theorem}[Newton's method has second order convergence]
      \label{thm_newtons_method_is_second_order}
      Let $f:\R\to\R$ be $C^1$ and $f''\in AC$.
      Let $(x_i)_{i\in\N}$ be a convergent sequence of iterates generated by
      Newton's method. Let $x_{\text{min}}=\min\{x_i:i\in\N\}$ and
      $x_{\text{max}}=\max\{x_i:i\in\N\}$.
      If there exists $C\in \R^+$ so that
      for all $x\in[x_{\text{min}}, x_{\text{max}}]$ and all $i\in\N$
      \[
        \abs{-\frac{1}{2}\frac{f''(x)}{f(x_i)}}\leq C
      \]
      then the sequence $(\epsilon_i)_{i\in\N}$ of errors,
      defined by $\epsilon_i=\alpha-x_i$ for all $i\in\N$,
      has second order convergence.
    \end{theorem}
    \begin{proof}
      Exercise. Literally follows from Lemma \ref{newton_basic_convergence_formula}
      once you understand the definitions.
    \end{proof}

    Theorem \ref{thm_newtons_method_is_second_order} has a lot of conditions
    that must be satisfied for second order convergence to be guaranteed.

    \begin{exercise}
      List the conditions of Theorem \ref{thm_newtons_method_is_second_order}.
      You should have four conditions.
      For each condition find a function $f:\R\to\R$ and a point $x_f$
      so that the sequence of errors generated by Newton's method is
      not second order convergent. The non-existence of the sequence counts
      as not second order convergent.
      You may not use a function given in class or in this chapter of the notes.
      No, it is not worth scouring the rest of the notes for an example, there
      aren't any. Yes, I checked.
    \end{exercise}

  \section{Secant method}

    Recall the definition of the derivative of a function.
    \begin{definition}
      A function $f:\R\to\R$ is differentiable at $x\in\R$ if and only if
      the limit
      \[
        \lim_{h\to 0}\frac{f(x+h)-f(x)}{h}
      \]
      exists and is finite.
    \end{definition}

    This implies that for some $h\in\R$ small enough
    that $\frac{f(x+h)-f(x)}{h}$ is a reasonable estimate for $f'(x)$.
    We do not, however, have any idea what the error in this estimate is.

    Despite that the estimate does, kinda, work if we use it to modify
    Newton's method.
    This can be useful if
    evaluation of the derivative is difficult or likely to have large error.
    Before I give the definition of the secant method I will 
    generalise the definition of iterates.

    \begin{definition}
      Let $m\in\N$ and
      Let $g:\R^m\to\R$. Let $x=(x_0,x_1,\ldots,x_{m-1})\in\R^m$
      the iterates (sometimes complex iterates - no relation to
      complex numbers) of $g$ from $x$
      is the sequence of points $(x_i)_{i\in\N}$ defined by
      \[
        x_{i+1}=g(x_i,x_{i-1},\ldots, x_{i-m}).
      \]
    \end{definition}

    \begin{definition}
      Let $f:\R\to\R$ and define $g:\R^2\setminus\{(x,x):x\in\R\}\to R$ by
      \[
        g(x,y)=x - f(x)\frac{x - y}{f(x) - f(y)} = \frac{yf(x) - xf(y)}{f(y)-f(x)}
      \]
      The secant method is the calculation of the iterates
      of $g$ from some $(x_0,x_1)\in\R$. 
    \end{definition}

    Of course problems occur for the secant method when $f(x_i)=f(x_{i-1})$,
    I'm going to ignore this.

    \begin{exercise}
      What possible things could you do when applying the secant method
      if $f(x_i)=f(x_{i-1})$?
    \end{exercise}

    \begin{exercise}
      Using the geometric interpretation of Newton's method and the method
      of false position what is the geometric interpretation of 
      the secant method?
    \end{exercise}

    A secant is a line which intersects a curve in two places. Thus, ``secant
    method'' is a method which uses a line joining two point

    \begin{lemma}
      Let $f:\R\to\R$.
      If $(x_i)_{i\in\N}$ is the iterates generated by the secant
      method from $(x_0,x_1)\in\R^2$ and there exists $\alpha\in\R$
      so that $x_i\to\alpha$ then $f(\alpha)=0$.
    \end{lemma}
    \begin{proof}
      Exercise.
    \end{proof}

    \begin{proposition}
      If the secant method produces a convergent sequence of
      iterates then they converge with order the golden ratio.
    \end{proposition}

  \section{When to stop iteration?}

    All iterative methods involve, a possibly, an infinite sequence of points.
    These infinite sequences eventually converge. Since we have finite
    computational time we need to stop the computational iteration at some point.
    When should that be?

    REFERENCE RELEVANT FIXED POINT THEOREM.

    Suggestions for Newton's method
    \[
      \frac{-f'(x_i)}{f(x_i)}<\epsilon,
    \]
    or
    \[
      \frac{x_{i+1}-x_i}{(x_{i} - x_{i-1})^2}\approx
        \frac{x_{i}-x_{i-1}}{(x_{i-1} - x_{i-2})^2}.
    \]
    Neither guarantee that we are close to a root - FIND EXAMPLE.
    Same goes for $x_i-x_{i-1}$ and
    $f(x_i)-f(x_{i-1})=0$ is scale invariant which implies it's completely useless.

