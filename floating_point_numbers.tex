\chapter{Floating point numbers}

  \begin{aquote}{{David Goldberg \cite{goldberg1996computer} in
    \cite[Section A.12]{hennessy2011computer}}}
    Arithmetic on Cray computers is interesting because it is driven by a
    motivation for the highest possible floating-point performance ...
    Addition on Cray computers does not have a guard digit,
    and multiplication is even less accurate than addition ...
    At least Cray computers serve to keep numerical analysts on their toes!
  \end{aquote}

  Almost all calculations that you perform in a computer will be done
  with floating point numbers. They are a nicely compact way to represent
  numbers in a computer, but as with all computer related things deeply
  and irrevocably flawed.

  Floating point notation is related to scientific notation.

  \begin{definition}
    Let $r\in\R$ the number $r$ is expressed in scientific notation
    when it is expressed as
    \[
      r = f\times 10^e,
    \]
    where $f\in(-10,10)$ and $e\in \R$.
  \end{definition}

  \begin{definition}\label{def_floating_point_number}
    A floating point number is a pair of real numbers
    $(e,f)\in\Z\times\R$, with an associated base $b\in\N$,
    excess $q\in\N$ and with $p\in\N$ digits so that
    \begin{enumerate}
      \item $b^pf\in\Z$,
      \item $-b^p<b^pf<b^p$,
      \item $\abs{f}<1$.
    \end{enumerate}
    The integer $e$ is called the exponent and
    the real number $f$ is called the fraction (or sometimes mantissa - Knuth
    has strong feelings about \emph{not} using the word mantissa
    \citep[Page 214]{knuth1998artvol2}).
    The value represented by a floating point number is
    \[
      f\times b^{e-q}.
    \]
  \end{definition}

  The requirement that $b^pf\in\Z$ requires that $f$ have $p$ digits
  when expressed in base $b$. 

  Most modern $64$-bit computers
  use $b=2$, $q=1023$, and $p=52$. 
  One bit is used to represent the sign so
  $64 - 52 - 1= 11$ bits are used for the exponent.
  For the purposes of this course we will always use
  $4$-decimal floating point numbers
  $b=10$, $q=0$, $p=3$.

  The excess is used to simplify certain algorithms as it is possible
  to require that $e>0$. Some texts, e.g.\ \cite{knuth1998artvol2},
  follow this convention. I do not as the point of this chapter is
  not a detailed understanding of how floating point numbers are handled
  by a computer, but rather an understanding of the errors that
  the use of floating point numbers causes.
  For this purpose the excess is only a source of unnecessary complication.
  So while we include it in formula where appropriate we also
  allow $e<0$ which, in effect means we can take $q=0$ with no loss of
  generality.
  
  \begin{exercise}
    Convert the following numbers from floating point representation
    to ``standard'' decimal notation and visa versa.
    \begin{enumerate}
      \item $(1, 0.1)$
      \item $3.14$
      \item $1000$
      \item $(2, 0.224)$
      \item $(-2,-0.1)$
    \end{enumerate}
  \end{exercise}

  \begin{exercise}
    Are the following valid floating point numbers, with our
    convention?
    \begin{enumerate}
      \item $(1, 1)$
      \item $(-9, 0)$
      \item $(0,0)$
      \item $(10,0.13)$
      \item $(0,1.13)$
    \end{enumerate}
  \end{exercise}

  \begin{exercise}
    Do the following numbers have floating point representations?
    \begin{enumerate}
      \item $0.1234$,
      \item $0.123$,
      \item $10.1$,
      \item $10.12$,
      \item $\pi$,
      \item $0.00005$.
    \end{enumerate}
  \end{exercise}

  \begin{definition}
    A floating point number $(e,f)$ is normalised if the most significant
    digit of $f$ is non-zero or if $f=0$ then $e$ takes its
    smallest possible value. 
    %If $f=0$ this is the same as requiring
    %\[
      %\frac{1}{b}\leq\abs{f}<1.
    %\]
  \end{definition}

  \begin{exercise}
    How many floating point numbers are there?
    How many floating point numbers are there that
    have the same value?
    How many normalised floating point numbers are there?
  \end{exercise}

  Most computers only use normalised floating point numbers. You are required
  to do the same.

  \begin{exercise}
    Write the following numbers in normalised floating point form.
    \begin{enumerate}
      \item $-0.0006$,
      \item $12.3$,
      \item $(-6,0.02)$,
      \item $(0,0.2)$.
      \item $0$.
      \item $-0$.
    \end{enumerate}
  \end{exercise}

  \section{Floating point representation error}

    Numerical error, also called rounding error,
    occurs when a computer attempts to assign a
    floating point representation to a number that does not have
    a floating point representation.

    \begin{definition}
      The floating point representation of
      a number $x\in\R$ is the floating point number $(e,f)$
      so that $\abs{x-fb^{e-q}}$ is minimised.
      The floating point representation of $x$ will be denoted
      $\fp{x}$.
    \end{definition}

    \begin{definition}\label{def_round}
      Given $x\in\R$ let $\round{x}$ be the value of
      the floating point number that represents $x$.
    \end{definition}

    \begin{definition}
      The numerical error, or rounding error, in the floating point
      representation of $x\in\R$ is $\abs{x-\round{x}}$.
    \end{definition}

    \begin{exercise}
      What are the floating point representations and errors
      of the following numbers?
      \begin{enumerate}
        \item $\pi$,
        \item $e$,
        \item $1/3$.
      \end{enumerate}
      Think about how a computer works... can you actually compute the
      errors?
    \end{exercise}

    \begin{definition}
      The decimal expansion of a real number $x\in\R$ is
      a sequence of numbers $(d_i)_{i\in\N}$, 
      $d_i\in\{0,1,\ldots,9\}$, with a largest integer subscript $i_\text{max}$ so that
      $d_{i_\text{max}}\neq 0$,
      so that
      \begin{align*}
        x &= \sum_{i}d_i\times 10^i\\
          &= \cdots + d_2100 + d_110 + d_0 + \frac{d_1}{10} + \frac{d_2}{100} + \cdots.
      \end{align*}
      The decimal expansion of $x$ is usually written
      $d_{i_\text{max}}d_{{i_\text{max}}-1}d_{{i_\text{max}}-2}\ldots d_1d_0\cdot d_{-1}d_{-2}\ldots$.
      This way of writing numbers is called positional notation, for
      obvious reasons.
    \end{definition}

    \begin{exercise}
      What are the decimal expansion, $(d_i)$, of the following numbers?
      What does the decimal expansion look like when written as
      $d_{i_\text{max}}d_{{i_\text{max}}-1}d_{{i_\text{max}}-2}\ldots d_1d_0\cdot d_{-1}d_{-2}\ldots$?
      \begin{enumerate}
        \item $1$,
        \item $10$,
        \item $12.34$,
        \item $\pi$,
        \item $e$.
      \end{enumerate}
    \end{exercise}

    \begin{definition}
      Given $x\in\R$ the floor of $x$, denoted
      $\floor{x}$ is the largest integer smaller than $x$.
    \end{definition}

    \begin{exercise}
      Show that if $x$ has the decimal expansion 
      $d_{i_\text{max}}d_{{i_\text{max}}-1}d_{{i_\text{max}}-2}\ldots d_1d_0\cdot d_{-1}d_{-2}\ldots$
      then the decimal expansion of $\floor{x}$ is
      $d_{i_\text{max}}d_{{i_\text{max}}-1}d_{{i_\text{max}}-2}\ldots d_1d_0$.
    \end{exercise}

    \begin{exercise}
      Prove that if $x\in\R$ has the decimal expansion
      $0\cdot d_{-1}d_{-2}\ldots$
      then
      the floating point representation of $x$, with our convention, is
      $(0, f)$ where $f$ has the decimal representation
      $0.e_{-1}e_{-2}e_{-3}$ where
      $e_{-1}\times 10^2 + e_{-2}\times 10^1 + e_{-1} =
        \floor{1000*x + 0.5}$.
    \end{exercise}

    Our particular choice for the floating point representation of
    an arbitrary real number is not unique. In particular this choice actually
    introduces some bias in the resulting numerical error. There are 
    ``better'' ways
    to do it, but for the purposes of this course this particular method will
    suffice. 

    \begin{definition}
      Let $x\in\R$ and let $(e,f)$ be its floating point representation.
      The absolute error of the floating point representation (or just
      absolute error) is $\abs{x - fb^{e-q}}$.
      The relative error of the floating point representation (or just
      relative error) is $\abs{\frac{x - fb^{e-q}}{x}}$.
    \end{definition}

    \begin{exercise}
      Calculate the relative and absolute error for the following numbers.
      \begin{enumerate}
        \item $1$,
        \item $0.1$,
        \item $1/3$,
        \item $1000/3$.
        \item $\pi$,
        \item $e$.
      \end{enumerate}
    \end{exercise}
    
    \begin{exercise}
      Let $x\in \R$ and suppose that there exists $\epsilon\in\R$ so that
      $\fpvalue{e}{f} +\epsilon = x$. What is the absolute and relative errors?
    \end{exercise}

    We can get estimates of the maximum error in floating point representations
    by calculating the distance between successive floating point numbers.

    \begin{exercise}\label{ex_distance_between_floating_point_numbers}
      Let $(e,f)$ be a floating point number, with $f\neq 0$. 
      What is the smallest floating
      point number larger than $(e,f)$? What is the largest floating point
      number smaller than $(e,f)$? What are the two distances between these numbers?
      How do these distances depend on $e$ and what does this imply about
      the difference in error between representations of ``small'' numbers
      and ``large'' numbers? What does this imply about how you should 
      use numbers in calculations?
    \end{exercise}

    \begin{definition}
      The number $0$ will always be represented by the floating point
      number $(-9,0)$.
    \end{definition}

    This particular choice, e.g.\ as opposed to $(0,0)$ or even $(1,0)$ and so on,
    is arbitrary. It is a reasonably standard choice as it simplifies certain
    algorithms in a computer.

    \begin{exercise}
      What is the smallest positive floating point number?
      What is the largest negative floating point number?
      What are the distances between these numbers and $0$?
      Why does this result not fit the pattern found in Exercise
      \ref{ex_distance_between_floating_point_numbers}?
    \end{exercise}

    \begin{lemma}\label{lem_absolute_error_in_fp_base10}
      Suppose that we are using floating point numbers with
      $b=10$, $q=0$ but $p$ arbitrary. Suppose also that
      we allow $t$ digits in the fractional part.
      If $x\in \R$ then
      \[
        \abs{x - \round{x}}\leq \frac{1}{2}\frac{\abs{x}}{10^{-t}}.
      \]
    \end{lemma}
    \begin{proof}
      Exercise. See \cite[Lemma 1.2.1]{isaacson2012analysis} for
      a detailed proof.
    \end{proof}
    \begin{proof}
      We can write $x$ as
      \[
        x = \pm 10^p(0.d_{-1}d_{-2}d_{-3}\cdots)
      \]
      and $\round(x)$ as
      \[
        \round(x) = \pm 10^p(0.\delta_{-1}\delta_{-2}\delta_{-3}\cdots\delta_{-t}).
      \]
      where
      \[
        \delta_{-1}\delta_{-2}\delta_{-3}\cdots\delta_{-t}
         = \floor{d_{-1}d_{-2}\cdots d_{-t}.d_{-t-1}d_{t-2}\cdots +\frac{1}{2}}.
      \]
      Note that this implies that
      \[
        \bigl(0.d_{-1}d_{-2}d_{-3}\cdots\bigr) -
          \bigl(0.\delta_{-1}\delta_{-2}\delta_{-3}\cdots\delta_{-t}\bigr)
          \leq \frac{1}{2}.
      \]
      The result is that
      \begin{align*}
        \abs{x-\round(x)} &= 10^{q-t}\left(0.d_{-t-1}d_{-t-2}\cdots\right)\\
          &\leq \frac{1}{2}10^{q-t}\\
          &\leq \frac{1}{2}10^{q-t}\frac{\abs{x}}{\abs{x}}\\
          &\leq \frac{1}{2}10^{1-t}\abs{x}.
      \end{align*}
    \end{proof}

    Recall the definition of $\round{x}$, Definition \ref{def_round}.

    \begin{lemma}[{\cite[Theorem 2.2]{higham2002accuracy}}]
      \label{lem_general_fp_representation_error}
      Let $x\in\R$. If 
      $b^{e-1}<\abs{x}<b^e$ then $\round{x}=x + \rho(x)$
      where $\abs{\rho(x)}<\frac{1}{2}b^{e-p}$ and
      if $\round{x}=x(1+\delta(x))$ then
      \[
        \abs{\delta(x)}=\frac{\abs{\rho(x)}}{\abs{x}}\leq
          \frac{\abs{\rho(x)}}{b^{e-1}+\abs{\rho(x)}}\leq
          \frac{\frac{1}{2}b^{e-p}}{b^{e-1}+\frac{1}{2}b^{e-p}}<
          \frac{1}{2}b^{1-p}.
      \]
    \end{lemma}

    \begin{exercise}
      Confirm that the previous two lemmas agree.
    \end{exercise}

    \begin{definition}
      The quantity 
      \[
        u=\frac{1}{2}b^{1-p}
      \]
      is called unit round off.
    \end{definition}

    \begin{exercise}
      Show that $u$ is the smallest normalised representable number.
    \end{exercise}

    \begin{definition}
      A subnormalised floating point number, $(e,f)$ is such that
      $e$ is as small as possible and $f=0.d_{-1}d_{-2}\cdots$
      is such that $d_{-1}=0$.
    \end{definition}

    \begin{exercise}
      Are subnormalised numbers normalised?
      Why should we both with subnormalised numbers?
    \end{exercise}

    \begin{exercise}
      Assume that $b=2$, $q=0$, $p=3$ and that the exponent can be
      $0,1,2$. Draw a horizontal line to represent
      the positive real numbers. Make marks in the line
      wherever there is a real number with an exact normalised
      floating point representation.
      Draw a new line and draw marks in that line wherever there
      is a real number that is representable as a subnormalised
      floating point number.
    \end{exercise}

  \section{Numeric operations}
    
    The numeric operations, $+,-,\times,\div$, do not
    behave as you expect on floating point numbers.

    \begin{lemma}
      \label{lem_fprep_operations_unit_roundoff}
      Prove the following
      \begin{enumerate}
        \item $\fp{x\pm y}=(x\pm y)(1+\phi 10^{-p})$,
        \item $\fp{xy}=xy(1+\phi 10^{-p})$, and
        \item $\displaystyle \fp{\frac{a}{b}}=\frac{a}{b}(1+\phi 10^{-p}),$
      \end{enumerate}
      where $\abs{\phi}\leq 5$.
    \end{lemma}
    \begin{proof}
      Use Lemma \ref{lem_absolute_error_in_fp_base10}.
    \end{proof}

    Lemma \ref{lem_fprep_operations_unit_roundoff} implies that
    the floating point representation of an operation on real numbers
    results in a maximum error of the unit round off $u$.
    What we need, however, is an estimate
    of the round off error that occurs
    when numerical operations on
    floating point numbers is compared to the result on the equivalent real
    numbers.

    \begin{definition}
      The symbols $+,-,\times,\div$ give the real value of
      their operation, as normal. Hence, if
      $x,y\in\R$ then $x+y\in\R$ and in particular
      $\fp{x}+\fp{y}\in\R$ it is not a floating point
      number.
      The symbols $\fpadd,\fpsub,\fptim,\fpdiv$, called
      the floating point arithmetic symbols, stand for
      the numeric operations whose result is a floating point
      number. Hence, if $x,y\in\R$ then
      $x\fpadd y=\fp{x+y}$ and $\fp{x}\fpadd\fp{y}=\fp{\fp{x}\fpadd\fp{y}}$
      and so on.
    \end{definition}

    \emph{Note: }
    The choice of $\fp{\cdot}$ or the floating point arithmetic symbols
    is stylistic and up to you. Pick whatever makes it easiest to
    read the expressions you write.

    Thus, for example, what would be good is if we can define
    $\fpadd$ so that
    \[
      x\fpadd y = \round{x + y},
    \]
    and similarly for $\fpsub,\fptim,\fpdiv$.

    This is 
    possible. 
    The algorithms to ensure that this is the case are long.
    Roughly the formulas for floating point arithmetic are in the following
    list. For the sake of simplicity I ignore renormalisation which is
    usually done after each operation in a computer, see
    \citep[Section 4.2.1]{knuth1998artvol2}.
    \begin{enumerate}
      \item $(e, f)\fpadd (g, h)=\left(e, f + \frac{h}{b^{e-g}}\right)$
        if $e>g$.
      \item $(e, f)\fpsub (g, h)=\left(e, f - \frac{h}{b^{e-g}}\right)$
        if $e>g$.
      \item $(e, f)\fptim (g, h)=\left(e + g, fh\right)$
      \item $(e, f)\fpdiv (g, h)=\left(e - g + 1, \frac{b^{-1}f}{h}\right)$.
    \end{enumerate}
    Lemma \ref{lem_general_fp_representation_error} can now be applied
    to prove that the so called ``Standard Model of Arithmetic'' holds.
    \begin{definition}[{\cite[Section 2.2]{higham2002accuracy}}]
      The standard model of arithmetic assumes that
      \begin{align*}
        x\fpadd y &= (x+y)(1+\delta),\\    
        x\fpsub y &= (x-y)(1+\delta),\\    
        x\fptim y &= (x\times y)(1+\delta),\\    
        x\fpdiv y &= (x\div y)(1+\delta),
      \end{align*}
      and that the floating point version of $\sqrt{\cdot}$ satisfies the
      same equality where
      where $\abs{\delta}<u$.
    \end{definition}
    I have not mentioned why you can assume this for $\sqrt{\cdot}$. You are free
    to look it up if you like.

    In order for these definitions of floating point operations
    to satisfy the standard model it is necessary to make use of
    at least $1$, and often up to $3$, extra bits. These extra bits
    are called \emph{guard bits} or guard digits. They are used to
    keep track of rounding during computation. To speed up computation
    some hardware don't contain guard digits (e.g.\ historical Cray computers).
    On such hardware it can be proven that the \emph{No Guard Digit Model}
    holds.
    \begin{definition}[{\cite[Section 2.4]{higham2002accuracy}}]
      The no guard digit model assumes that
      \begin{align*}
        x\fpadd y &= x(1+\delta) + y(1+\beta),\\    
        x\fpsub y &= x(1+\delta) - y(1+\beta),\\    
        x\fptim y &= (x\times y)(1+\delta),\\    
        x\fpdiv y &= (x\div y)(1+\delta),
      \end{align*}
      where $\abs{\delta},\abs{\beta}<u$.
    \end{definition}
    Essentially a lack of guard digit prevents us from using rounding.

    It is possible for the result of a floating point operation to be an
    invalid floating point number. Different CPU's handle this in
    different ways. The industry standard should be for an
    exception to be raised so that the software requesting the result
    of the operation and make an informed decision about what to
    do next. 
    Something called the IEEE standard 754 specifies the default behaviour 
    for floating point operations that do not result in floating point
    numbers. Table \ref{table_IEEE754_FP_results} lists the
    default behaviour given by IEEE standard 754. Almost all computers that
    you use will conform to this standard.

    \begin{definition}
      An overflow occurs when the exponent in the result of a floating
      point operation is too large.
      An underflow occurs when the exponent in the result of a floating
      point operation is too small.
    \end{definition}

    \begin{table}[hb]
      \centering
      \begin{tabular}{lll}
        Exception & Example & Default Result\\
        \midrule
        Invalid operation & 
          $0\fpdiv 0$, $0\times\infty$, $\sqrt{-1}$ &
            Not a number \\
        Overflow & & $\pm\infty$ \\
        Divide by zero 
          & finite number / 0
            & $\pm\infty$ \\
        Underflow & & Subnormal numbers\\
      \end{tabular}
      \caption{A table giving the IEEE standard 754 results for floating 
        point operations that do not result in normalised floating point
        numbers}
      \label{table_IEEE754_FP_results}
    \end{table}

    \begin{exercise}
      Perform the following calculations and identify any cases of
      over / underflow.
      \begin{enumerate}
        \item $1+ 2$,
        \item $1\fpadd 2$,
        \item $100,000\times 100,000$.
        \item $100,000\fpdiv 100,000$.
        \item $0.0001\fpdiv 100,000,000$.
      \end{enumerate}
    \end{exercise}

    Addition, subtraction, multiplication and division in a computer
    are the operations $\fpadd,\fpsub,\fptim,\fpdiv$ not the
    operations $+,-,\times, \div$.

    \begin{exercise}
      Why do computers use
      $\fpadd,\fpsub,\fptim,\fpdiv$ rather than
      $+,-,\times, \div$?
    \end{exercise}

    Floating point arithmetic does not behave like normal arithmetic.
    Some identities still hold, others do not. Strict proofs of the
    results below require detailed analysis of the algorithms used
    to determine the result of each floating point operation. We will
    not be discussing these algorithms. 
    Never-the-less, exploration - and proof where possible - of the
    identities below will help you understand where floating point
    operations differ from the standard arithmetic operations.

    \begin{exercise}
      Assume that the result of a floating point operation that underflows
      is $\fp{0}$. 
      \begin{enumerate}
        \item Find three numbers $x,y,z\in\R$ so that
          $(x\fptim y)\fptim z = 0 $ but
          $x\fptim (y\fptim z) \neq 0 $.
        \item Find five positive numbers $a,b,c,d,y\in\R$ so that
          \begin{align*}
            (a\fptim y \fpadd b)\fpdiv(c\fptim y\fpadd d)\approx\frac{2}{3}\\
            (a\fpadd b\fpdiv y)\fpdiv(c\fpadd d\fpdiv y)=\fp{1}.
          \end{align*}
          Compare this to
          \[
            \frac{ay + b}{cy+d}=\frac{a + \frac{b}{y}}{c + \frac{d}{y}}.
          \]
      \end{enumerate}
      If subnormal numbers are allowed can a similar situation result?
    \end{exercise}

    \begin{lemma}[{\cite[Section 4.2.2.A]{knuth1998artvol2}}]
      Assuming that under and overflows do not occur,
      if $x,y,z\in\R$ then
      \begin{enumerate}
        \item $x\fpadd y=y\fpadd x$,
        \item $x \fpsub y=x\fpadd -y$,
        \item $x\fpadd y=0$ if and only if $x=-y$,
        \item $x\fpadd 0 = x$,
        \item $x\fpsub y = -(y\fpsub x)$,
        \item if $x\leq y$ then $x\fpadd z\leq y\fpadd z$,
        \item $x\fptim y= y\fptim x$,
        \item $(-x)\fptim y=-(x\fptim y)$,
        \item $1\fptim y=y$,
        \item $x\fptim y=0$ if and only if $x=0$ or $y=0$,
        \item $(-x)\fpdiv y=u\fpdiv(-y)=-(x\fpdiv y)$,
        \item $0\fpdiv y=0$,
        \item $x\fpdiv 1=x$,
        \item $x\fpdiv x=1$,
        \item if $x\leq y$ and $z>0$ then $x\fptim z\leq y\fptim z$,
        \item if $x\leq y$ and $z>0$ then $x\fpdiv z\leq y\fpdiv z$,
        \item if $0<x\leq y$ then $w\fpdiv x\geq w\fpdiv y$,
        \item if the value of $x\fpadd y$ is exactly $x + y$ then
          $\left(x\fpadd y\right)\fpsub y=x$,
        \item if the value of $x\fptim y$ is exactly $x\times y$ which is
          not equal to zero then
          $(x\fptim y)\fpdiv y = x$.
      \end{enumerate}
    \end{lemma}

    \begin{exercise}
      Find $x,y,z\in\R$ so that
      \begin{enumerate}
        \item $(x\fptim y) \fpadd (x\fptim z)\neq x\fptim(y\fpadd z)$.
          Compare with 
          $xy + xz = x(y+z)$, and
        \item $x\fptim(y\fptim z)\neq (x\fptim y)\fptim z$,
      \end{enumerate}
      without under or overflows occurring.
    \end{exercise}

    \begin{exercise}
      Recall that $b\in \N$ is the base of the floating point number system.
      Show that for all $x,y\in\R$,
      $b\fptim(x\fpadd y)=(b\fptim x)\fpadd(b\fptim y)$, if under / overflow
      does not occur.
    \end{exercise}

  \section{Examples of costly numerical problems}

    Ariane 5 rocket exploded $\approx 40$ seconds after launch on June 4
    1996. The destroyed rocket and cargo cost about \$500 million US.
    Development cost for the rocket was about \$7 billion US.
    The cause of failure was tracked down overflow 
    due to a floating point number
    (in 64 bits) being converted to a 16 bit signed integer.
    The same software, however, had been used successfully in the Ariane 4
    rockets. The overflow occurred because the Ariane 5 rocket travelled
    significantly faster than Ariane 4. 
    See \url{https://en.wikipedia.org/wiki/Ariane_5#Notable_launches}
    for more details.

    In 2015 it was discovered that a similar bug exists in the 747 engine 
    control software. Fail sages would ensure that the engines switch off 
    (possibly mid flight) if the engines were left running for 248. 
    The number of days, 248, expressed in 100ths of a second
    is 2147483647 which is roughly the maximum value of a 32 bit signed
    integer.
    See \url{https://www.theguardian.com/business/2015/may/01/us-aviation-authority-boeing-787-dreamliner-bug-could-cause-loss-of-control}

    Ever played Civilisation and had Ghandi as an opponent? The
    reason he becomes hyper aggressive is due to an overflow.
    See \url{https://www.geek.com/games/why-gandhi-is-always-a-warmongering-jerk-in-civilization-1608515/}.

    The millenium bug was also due to overflow. 
    See \url{https://en.wikipedia.org/wiki/Year_2000_problem}.
    Estimated total cost in 2018 US dollars is \$448 million US.
    There are plenty of other ``Millenium Bugs'' waiting for us.
    The next one will occur on January 19th 2038, 
    \url{http://maul.deepsky.com/~merovech/2038.html}.

    In 2012 a 104 year old Swedish women was sent a letter asking her
    to enrol in pre-school. The reason? Overflow
    \url{https://www.independent.co.uk/news/world/europe/swedish-pensioner-aged-104-offered-kindergarten-place-due-to-computer-glitch-a6967786.html}.

    The Pentium FDIV bug, \url{https://en.wikipedia.org/wiki/Pentium_FDIV_bug}, 
    is an error in the floating point arithmetic in early Pentium chips.
    Basically a calculation didn't confirm to the standard model and
    more error than expected was introduced. Fixing it cost
    Intel \$475 million US in 1995 dollars.

    During the Gulf War (the first one) American army barracks used
    banks of patriot missiles to detect and shoot down incoming missiles.
    Due to the length of time a particular missiles battery had been left running
    the batteries internal clock had accumulation about 0.34 secs of error.
    This meant that software predictions of where incoming missiles would be
    were wrong by about 500 meters. The result was that 28 people died
    and roughly another 100 were wounded. All because of numerical error.
    See \url{http://www-users.math.umn.edu/~arnold//disasters/patriot.html}.

    On August 23 1991 a Swedish engineering company paid out around
    \$1 billion US in compensation for the catastrophic failure of
    an off shore oil rig. The error was determined to be a software bug
    in the code that performed simulations of the sea. 
    See \url{http://www-users.math.umn.edu/~arnold//disasters/sleipner.html}.

    There are many many many more examples of similar things happening.
    A few careful google searches will provide endless examples.
    My main advice is: if you work in a company that uses Excel to 
    for commercially sensitive or critical calculations then that company
    is a bankruptcy waiting to happen. This isn't even because Excel is bad
    at calculations it is because Excel does little to prevent people
    from introducing errors.
    Others have plenty more to say about this;
    \url{https://www.forbes.com/sites/timworstall/2013/02/13/microsofts-excel-might-be-the-most-dangerous-software-on-the-planet/}, \url{https://blog.floatapp.com/5-greatest-spreadsheet-errors-of-all-time/}.

    More examples; \url{http://ta.twi.tudelft.nl/users/vuik/wi211/disasters.html}.

  \section{Revision questions}

    \begin{exercise}
      Representation of numbers on a computer is a big deal, there are \emph{a lot}
      of ways to do it. Floating point numbers, specifically the form of
      floating point numbers I've described, is only one way.
      Can you think of a way to represent numbers in a computer that is better
      than floating point numbers? For the purpose of this discussion you
      can assume that memory comes in blocks of $8$ decimal digits.
      If you wish to count operations assume that any arithmetic operation
      on a block of $8$ decimal digits is one operation.
    \end{exercise}

    \begin{exercise}
      Definition \ref{def_floating_point_number} makes no claim to limit the
      size of $e$ or of $f$. Why then do we have round off error in
      computers?
    \end{exercise}

    \begin{exercise}
      Show that Cauchy's inequality in $\R$ fails for floating point arithmetic.
      What does this imply about a computers ability to replicate mathematical
      arguments? Use your example to show that the standard deviation of some
      observations can, in a computer, result in taking the square root of
      a negative number. The formula for the standard deviation, with
      normal arithmetic operators is
      \[
        \sqrt{\frac{n\sum x_i^2 - \left(\sum x_i\right)^2}{n(n-2)}},
      \]
      where $n$ is the number of observations and the sums are over
      $i=1,\ldots, n$.

      Show that the following recursive formula produces the standard deviation,
      \citep[Pages 419---420]{welford1962note}.
      \begin{align*}
        M_1 &= x_1,\\
        S_1 &= 0,\\
        M_k &= M_{k-1}\fpadd(x_k\fpsub M_{k-1})\fpdiv k,\\
        S_k &= S_{k-1}\fpadd (x_k\fpsub M_{k-1})\fptim (x_k\fpsub M_k).
      \end{align*}
    \end{exercise}

    Using Lemma \ref{lem_general_fp_representation_error} it is possible 
    to derive bounds on the amount of error that arises in floating
    point operations. 
    For example $x\fpadd y=x+y+\delta(x+y)$ where
    we have a bound on $\delta$.
    If $x,y,z\in\R$ then
    \[
      (x\fptim y)\fptim z=xy(1+\delta_1)\fptim z=xyz(1+\delta_1)(1+\delta_2)
    \]
    and
    \[
      x\fptim( y\fptim z)=x\fptim yz(1+\delta_3)=xyz(1+\delta_3)(1+\delta_4),
    \]
    where $\abs{\delta_i}<\frac{1}{2}b^{1-p}$, $i=1,2,3,4$, provided no
    under or overflow occurs.
    Hence
    \[
      \frac{(x\fptim y)\fptim z}{x\fptim( y\fptim z)}=
        \frac{(1+\delta_1)(1+\delta_2)}{(1+\delta_3)(1+\delta_4)}
        =1+\delta,
    \]
    where
    \[
      \abs{\delta}<\frac{2b^{1-p}}{\left(1-\frac{1}{2}b^{1-p}\right)^2}.
    \]
    
    There is vastly more theory regarding the analysis of floating point
    numerical operations. But we shall stop here.

    \begin{exercise}
      Let $p(x)=(x-a)(x-b)$ be a polynomial. Which of the following
      formulas for evaluation produce the smallest error when implemented
      in a computer,
      \begin{enumerate}
        \item $(x-a)(x-b)$,
        \item $x^2 - (a+b)x + ab$, or
        \item $x(x-(a+b)) + ab$?
      \end{enumerate}
      How should you implement the evaluation of polynomials in a computer?
    \end{exercise}

    \begin{exercise}
      Let $x,y,u,v\in\R$ and suppose that $x\geq 0$ and $y\geq 0$.
      Show that $(u\fpadd x)\fpadd(v\fpadd y)\geq u\fpadd v$.
    \end{exercise}

    \begin{exercise}
      Find $x,y,z\in\R$ so that
      $(x\fptim(y\fptim z))$ overflows but
      $((x\fptim y)\fptim z$ does not.
    \end{exercise}
    
    \begin{exercise}
      Is $x\fpdiv y = x\fptim (1\fpdiv y)$ an identity for all floating point
      numbers $x$ and $y\neq 0$ such that no over or underflow occurs?
      What does this tell you about the inverse of a number when programming?
    \end{exercise}

    \begin{exercise}
      Are either of the following identities valid for all floating point numbers
      \begin{enumerate}
        \item $0\fpsub(0\fpsub x)=x$,
        \item $1\fpdiv(1\fpdiv x)=x$?
      \end{enumerate}
      What does this say about how to write equations in a computer?
    \end{exercise}

    \begin{exercise}
      Does $x\leq y$ imply that $x\leq (x\fpadd y)\fpdiv 2\leq y$? How
      does the answer depend on the base?
      What about $x\leq x\fpadd((y\fpsub x)\fpdiv 2)\leq y$?
    \end{exercise}

    \begin{exercise}[{\cite[Problem 2.8]{higham2002accuracy}}]
      What is the result of performing $\sqrt{1-10^{-4}}$ in our agreed
      floating point arithmetic? What about $\sqrt{1-2^{-53}}$ on a computer?
    \end{exercise}

    \begin{exercise}[{\cite[Problem 1.3.1]{isaacson2012analysis}}]
      Let $p(x)= x^2 + 2bx + x$.
      Prove that sequence of points $(x_i)_{i\in\N}\subset\R$ defined by
      \[
        x_{i}=-\frac{c}{2b}-\frac{x_{i-1}^2}{2b}
      \]
      converges to the smaller of the two roots.
      Similarly, prove that 
      sequence of points $(x_i)_{i\in\N}\subset\R$ defined by
      \[
        x_{i}=x_{i-1}-\frac{x_{i-1}^2 + 2bx_{i-1} + c}{2x_{i-1} + 2b}
      \]
      converges to the smaller of the two roots.
      By using the standard model analyse the error properties of the two schemes.
      Using our agreed floating point standard perform enough iterates of
      both schemes to numerically verify your theoretical work.
      To do this assume that $c=-1$ and $b=-100$.
    \end{exercise}

    \begin{exercise}
      Let $n=10^6$
      for each $i=1,\ldots n$ let $x_i=10/9$. What is the result of
      the computation $(\ldots((x_1\fpadd x_2)\fpadd x_3)\fpadd \ldots x_n)$?
      What happens during the calculation of the standard deviation
      of the $x_i$'s when using the standard formula?
      What happens when using the recursive formula? Prove that
      $S_i\geq 0$ for all $i$.
    \end{exercise}

    \begin{exercise}
      When people talk about inaccuracy in floating point arithmetic they often
      ascribe errors to ``cancellation'' that occurs during the subtraction of nearly equal
      quantities. But when $x,y$ are approximately equal, the difference
      $x\fpsub y$ is obtained exactly, with no error. What do these people
      really mean?
    \end{exercise}

    \begin{exercise}
      Prove that $1\fpdiv (1\fpdiv (1\fpdiv x))=1\fpdiv x$ for all $x\neq 0$.
    \end{exercise}

    \begin{exercise}[{\citep[Lemma 1.2.2]{isaacson2012analysis}}]
      Using Lemma \ref{lem_absolute_error_in_fp_base10}
      and the identities
      \begin{align*}
        \fp{xy + uv}&=\bigl(xy(1+\phi_110^{-p}) + uv(1+\phi_210^{-p})\bigr)
          (1+\phi_310^{-p}),\\
        \fp{\sum_{i=1}^nx_iy_i}&=\fp{\fp{\sum_{i=1}^{n-1}x_iy_i} + \fp{x_ny_n}},
      \end{align*}
      show that if $n 10^{1-p}\leq 1$ then
      \[
        \fp{\sum_{i=1}^{n} x_iy_i} = \sum_{i=1}^n(x_i+\delta_i)y_i,
      \]
      where $\abs{\delta_1}\leq n\abs{x_1}10^{1-p}$ and
      $\abs{\delta_i}\leq (n-i+2)\abs{x_i}10^{1-p}$ for
      $i=2,\ldots,n$.
    \end{exercise}

    \begin{exercise}[{\cite[Problem 4.2.2.30]{knuth1998artvol2}}]
      Let 
      \[
        f(x)=\frac{1-x^{107}}{1-x}=1+ x+ x^2 + \cdots x^{106}
      \]
      and let 
      \[
        g(x)=f\left(\left(\frac{1}{3}-x^2\right)\left(3 + 3.45x^2\right)\right),
      \]
      defined for $x\in(0,1)$.
      What are the values of $g(10^{-i})$ for $i=3,4,5,6$? Does a computer give
      difference answers to a pocket calculator? Do different programming languages
      or compilers give different answers?
      Given that
      \[
        g(\epsilon)=107 - 10491.35\epsilon^2 + 659749.9625\epsilon^4
          - 30141386.26625\epsilon^5 + O(\epsilon^8)
      \]
      which way to calculate values is most accurate?
    \end{exercise}

    \begin{exercise}[{\cite[Problem 4.2.2.31]{knuth1998artvol2}}]
      The polynomial $2y^2+9x^4-y^4$ is approximately
      $-3.7\times 10^{19}$ at $x=408855776$ and $y=708158977$ on most modern
      $64$-bit machines.
      The polynomial $2y^2+(3x^2 - y^2)(3x^2 + y^2)$ is approximately
      $1.0\times 10^{18}$ at the same point.
      Prove that the two polynomials are equal and that the correct value
      of the polynomial at the point is $1$. 
      How can you construct a similar example?
    \end{exercise}

    \begin{exercise}[{\citep[Section 1.11]{higham2002accuracy}}]
      We know that $e=\lim_{n\to\infty}(1+1/n)^n$. Is this a reasonable
      way to calculate $e$?
      Make a table of the results for $n=10^i$ for $i=1,2,\ldots, 7$.
      Do you observe stability? Were there any subtractions?
    \end{exercise}

    \begin{exercise}[{\cite[Section 4.2.2, Theorems A, B and C]{knuth1998artvol2}}]
      Prove that 
      \[
        \left((u\fpadd v)\fpsub u\right) + 
          \left((u\fpadd v)\fpsub\left((u\fpadd v)\fpsub v\right)\right)=u\fpadd v,
      \]
      assuming that no under or overflow occurs.
      Let 
      \begin{align*}
        u'&=(u\fp add v)\fpsub v, & v''&=(u\fpadd v)\fpsub u'.
      \end{align*}
      Use the equation above to prove that
      \[
        u + v = (u\fpadd v) + \left((u\fpsub u')\fpadd (v\fpsub v'')\right).
      \]
      This is an equation, exact in the real numbers, that can be used
      to determine the error of addition. This is used in real life programs
      to estimate total error during computation. For some algorithms,
      e.g.\ ``brentq'', such error estimates determine what the algorithm does.
      Prove that if $b=2,3$ and $\abs{u}\geq \abs{v}$ then
      \[
        u+v = (u\fpadd v) + \left(u\fpsub(u\fpadd v)\right)\fpadd v.
      \]
    \end{exercise}

    \begin{exercise}[{\cite[Exercise 13.2]{trefethen1997numerical}}]
      To answer this question
      work in this floating point system.
      \begin{itemize}
        \item The floating point system in a modern $64$ bit machine has
          $b=2$, $p=52$ and $11$ bits for the exponent. With respect to
          this floating point system what is the largest representable integer?
        \item Given an explicit formula for the largest representable integer
          with respect to any floating point system.
        \item Let $n$ be the largest representable integer.
          Write and run a program that produces evidence that $n$, $n-2$,
          and $n-3$ are representable but $n+1$ is not.
          What does the evidence say about $n+2$, $n+3$, and $n+4$?

          \emph{Note:} Answers to this question will depend on the programming
          language that you use. I recommend try two different languages like
          Python and C. Even with C the results that you get will depend on
          the particular C compiler that you use.
      \end{itemize}
    \end{exercise}




