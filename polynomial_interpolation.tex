\chapter{Polynomial interpolation}
  
  The rest of the course will rely, in one way or another, on techniques
  related to polynomial interpolation. Along with matrix methods for
  linear 
  equations and iterative methods for non-linear equations polynomial
  interpolation is the third load baring wall that supports almost all
  numerical methods.

  \section{Some basic results}

    Uniqueness of polynomials through points.
    \begin{lemma}
      \label{lem_uniqueness_poly}
    \end{lemma}

    \begin{theorem}[{Rolle's Theorem, \cite[Theorem A.2]{suli2003introduction}}]
      If $f:[a,b]\to\R\in C^1([a,b])$ is such that
      $f(a)-f(b)=0$ then there exists
      $\xi\in(a,b)$ so that $f'(\xi)=0$.
    \end{theorem}
    \ben{proof?}
    
    \ben{generalised rolle's theorem}

  \section{Lagrange polynomials}

    \ben{double check this material against existing MATH131 notes}

    \begin{definition}
      If $n\in N$ and $x_i\in\R^n$, $i=0,\ldots,n$ are $n$ distinct numbers
      then the polynomial
      \[
        L_k(x)=\prod_{i\neq k}\frac{x-x_i}{x_k-x_i}
      \]
      is the $k$\textsuperscript{th} Lagrange polynomial
      on $x_1,\ldots, x_n$.
    \end{definition}

    \begin{exercise}
      Show that $L_k(x_k)=1$ and $L_k(x_i)=0$ if $i\neq k$.
    \end{exercise}

    \begin{proposition}[{\cite[Theorem 6.1]{suli2003introduction}}]
      \label{prop_lagrange_interpolation}
      If $n\in N$, $x_i\in\R^n$ are $n$ distinct numbers
      and $y_i\in\R^n$ are $n$ numbers then the
      polynomial  
      \[
        p(x)=\sum_{i=0}^nL_i(x)y_i
      \]
      is the unique degree $n$ polynomial $p(x)$ so that
      $p(x_i)=y_i$.
    \end{proposition}
    \begin{proof}
      Exercise. This follows directly from the definition of Lagrange 
      polynomials and Lemma \ref{lem_uniqueness_poly}.
    \end{proof}
  
    \begin{definition}
      The polynomial $p(x)$ given in Theorem \ref{prop_lagrange_interpolation}
      is the Lagrange interpolating polynomial through the
      points $\{(x_i,y_i)\in\R^2:i=0,\ldots,n\}$.
      The points $\{x_i\in\R:i=0,\ldots,n\}$ are the interpolation points
      for $p$.
    \end{definition}

    \begin{exercise}[{\cite[Exercise 6.1]{suli2003introduction}}]
      What is the Langrange interpolating polynomial of $x\mapsto \exp(x)$
      through $\{-1,0,1\}$?
    \end{exercise}

    \begin{theorem}
      \label{thm_wietrass_polynomial_interpolation}
    \end{theorem}

    \begin{definition}
      \label{def_error_polynomial_lagrange}
      Let $\{x_0,\ldots,x_n\}\subset\R$ define
      \[
        \omega(x)=\prod_{i=0}^n(x-x_i).
      \]
    \end{definition}

    Of course $\omega$ is closely related to Lagrange polynomials the
    actual formula relating the two is the unfortunate
    \[
      L_k(x)=\lim_{u\to x_k}\frac{u-x_k}{x-x_k}\frac{\omega(x)}{\omega(u)}.
    \]
    It'd be nicer to write
    \[
      L_k(x)=\frac{x_k-x_k}{x-x_k}\frac{\omega(x)}{\omega(x_k)},
    \]
    but this isn't a valid expression as the term $(x_k-x_k)$ must be 
    interpreted as cancelling the same term in $\omega(x_k)$.

    \begin{theorem}[{\cite[Theorem 6.2]{suli2003introduction}}]
      \label{thm_lagrange_error_bounds}
      Let $n\geq 0$, $f:[a,b]\to\R\in C^{n+1}([a,b])$,
      $\{x_0,\ldots,x_n\}\subset[a,b]$ and let
      $\omega$ be as in Definition \ref{def_error_polynomial_lagrange}.
      If $p:[a,b]\to\R$ is the Lagrange polynomial through
      $\{x_0,\ldots,x_n\}$ then
      for all $x\in[a,b]$ there exists $\xi(x)\in(a,b)$ so that
      \[
        f(x)-p(x) = \frac{f^{(n+1)}(\xi(x))}{(n+1)!}\omega(x).
      \]
    \end{theorem}
    
    \begin{corollary}
      With the assumptions of Theorem \ref{thm_lagrange_error_bounds},
      if
      \[
        M=\max\{\abs{f^{(n+1)}(x)}:x\in[a,b]\}
      \]
      then
      \[
        \abs{f(x)-p(x)} \leq \frac{M}{(n+1)!}\abs{\omega(x)}.
      \]
    \end{corollary}

    \begin{exercise}
      Let $p:[-1,1]\to\R$ be the Lagrange interpolating polynomial
      for $f(x)=\exp(x)$ through $\{-1,0,1\}$.
      Write down the formula for
      \[
        e_1(x)=\frac{f^{(n+1)}(\xi(x))}{(n+1)!}\omega(x).
      \]
      Where is the maximum error?
      What is
      \[
        M=\max\{\abs{f^{(n+1)}(x)}:x\in[a,b]\}\text{?}
      \]
      Write down the formula for
      \[
        e_2(x)=\frac{M}{(n+1)!}\abs{\omega(x)}.
      \]
      Plot $\abs{e_1-e_2}$. Is $e_2$ a reasonable approximation to
      the actual truncation error?
    \end{exercise}

    \begin{proof}[{Proof of Theorem \ref{thm_lagrange_error_bounds}}]
      The equation 
      \[
        f(x)-p(x) = \frac{f^{(n+1)}(\xi(x))}{(n+1)!}\omega(x).
      \]
      clearly holds when $x=x_i$, $i=0,\ldots,n$. Suppose then
      that $x\in[a,b]\setminus\{x_0,\ldots,x_n\}$ and let
      \[
        \phi(t) = f(t) - p(t) - \frac{f(x)-p(x)}{\omega(x)}\omega(t).
      \]
      For all $i=0,\ldots,n$, $\phi(x_i)=0$ and $\phi(x)=0$.
      Thus $\phi$ has $n+2$ roots
      \ben{generalised Rolles}
    \end{proof}

    Increasing the number of interpolation points with the idea that
    a better approximation by Lagrange polynomials will result does not
    always work.

    \begin{exercise}
      Let $f:[-5,5]\to\R$ be defined by
      \[
        f(x)=\frac{1}{1+x^2}.
      \]
      For each $n=2,\ldots, 25$ plot the Lagrange interpolating
      polynomial and show that there is an exponential-like divergence
      in the maximum error.
    \end{exercise}

  \section{Hermite polynomials}

    If in addition to having the interpolation points and the value of
    the function at these points, we also have the value of the
    derivative of the function at these points then we can build better
    approximations.

    \begin{theorem}[{\cite[Theorem 6.3]{suli2003introduction}}]
      If $n\geq 0$
      $\{x_0,\ldots, x_n\}\subset\R$ and
      $y_i,z_i\in\R$ are a series of points then the unique
      polynomial $p:\R\to\R$ so that
      $p(x_i)=y_i$ and $p'(x_i)=z_i$
      is given by
      \[
        p(x)=\sum_{i=0}^n\left(H_k(x)y_i+K_k(x)z_i\right),
      \]
      where
      \begin{align*}
        H_k(x) &= \bigl(L_k\bigr)^2\bigl(1 - 2 L_k'(x_k)(x-x_k)\bigr),\\
        K_k(x) &= \bigl(L_k\bigr)^2\bigl(x-x_k\bigr).
      \end{align*}
    \end{theorem}

    \ben{include more Hermite polynomial stuff}
    \ben{include MATH321 notes}

  \section{Multidimensional polynomial interpolation}

  \section{Radial basis functions}

    
    

    
  
