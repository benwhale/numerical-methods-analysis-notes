\chapter{Ordinary differential equations - boundary value problems}
  
  A boundary value problem is a system of ordinary differential equations
  requiring more than one initial value that are specified at different points.

  \begin{definition}
    \label{def_bvp}
    Let $o\in\N$, $o > 1$,
    $y:\R \to \R^n$ 
    $f:\R\times\R^n\to\R^n$ be functions so that
    \begin{equation}
      \label{equ_basic_bvp}
      \left.\frac{d^o}{dx^o}y\right|_{x=a}a=
        f\left(a,y(a),\left.\frac{d}{dx}y\right|_{x=a}, \ldots, 
          \left.\frac{d^{o-1}}{dx^{o-1}}y\right|_x=a\right).
    \end{equation}
    The boundary values for $y$ 
    is a set of $3$-tuples $(x_i, y_i, o_i)$.
    The boundary values and Equation \ref{equ_basic_bvp}
    specify a boundary value problem.
    A function $y:\R\to\R^n$ satisfies the boundary value problem if
    Equation \ref{equ_basic_bvp} holds and for all 
    $3$-tuples in the set of boundary values
    \[
      \left.\frac{d^{o_i}}{dx^{o_i}}y\right|_{x=x_i}=y_i
    \]
  \end{definition}

  \begin{exercise}
    Why is $o>1$ in Definition \ref{def_bvp}?
  \end{exercise}

  \section{Shooting method}

    The shooting method converts a boundary value problem into 
    an initial value problem.
    Consider the following boundary value problem
    Let $y:[a,b] \to \R^n$ and
    $f:\R\times\R^n\to\R^n$ be functions so that
    \begin{align*}
      \label{equ_shooting_method_bvp}
      \left.\frac{d^2}{dx^2}y\right|_{x}a&=f\left(x,y(x)\right),\\
      y(a)&=a_0,\\
      y(b)&=b_0.
    \end{align*}

    This will be equivalent to the initial value problem,
    \begin{align}
      \label{equ_shooting_method_ivp}
      \left.\frac{d^2}{dx^2}y\right|_{x}a&=f\left(x,y(x)\right),\\
      y(a)&=a_0,\\
      \left.\frac{d}{dx}y\right|_{x=a}=t,
    \end{align}
    for some value of $t$. Denote $y(t; x)$ the solution to
    Equation \ref{equ_shooting_method_ivp}. Then there is a
    function $\phi:\R\to\R$ given by $\phi(t)=y(t; b)$.
    If we knew $\phi$ then we could calculate $t$ as
    $\phi^{-1}(b)$. Of course usually we don't know $\phi$ in enough
    detail to find an explicit inverse. 

    We can numerically calculate $\phi(t)$ by solving
    Equation \ref{equ_shooting_method_ivp} to find $y(t; b)$.
    Since we need to solve $\phi(t)=b$ an numerical method
    to solve this non-linear system of equations will do the job.

    Newton's method for example requires calculation of $\frac{d}{dx}\phi$.
    We can get access to this derivative via the defining differential equations,
    \begin{align*}
      \frac{d^2}{dx^2}\frac{d}{dt}y(t;x)
      \frac{d}{dt}\frac{d^2}{dx^2}y(t;x)
        &= \frac{d}{dt}f(x,y(t;x))=\frac{d}{dy}f(x,y(t;x))\frac{d}{dx}y(t;x),\\
      \frac{d}{dt}y(t;a)&=0,\\
      \frac{d}{dx}\frac{d}{dt}y(t;a)=
        &=\frac{d}{dt}\frac{d}{dx}y(t;a)=1.
    \end{align*}
    This gives a second order initial value problem for 
    $\frac{d}{dt}\phi(t)=\frac{d}{dt}y(t;x)$.

    Thus we have two initial boundary value problems, one that we can use 
    to calculate $\phi(t)$ and one that allows us to calculate $\phi'(t)$.
    With these values we can use Newton's method to solve $\phi(t)=b_0$.
    Once we have a solution to $\phi(y)=b_0$, to some appropriate order of 
    accuracy we will also have a solution to the original boundary value problem.

    \begin{exercise}
      Solve the following boundary value problem using the shooting method.
      Let $y:[-2,2] \to \R^n$ be such that
      \begin{align*}
        \left.\frac{d^2}{dx^2}y\right|_{x}a&=\exp(-x^2)
        y(-2)&=1,\\
        y(2)&=1.
      \end{align*}
      Plot $\phi(t)$. Is there a solution to the boundary value problem
      \begin{align*}
        \left.\frac{d^2}{dx^2}y\right|_{x}a&=\exp(-x^2)
        y(-2)&=1,\\
        y(2)&=2.
      \end{align*}
      Prove that there is / isn't?
      If there is a solution calculate it. If there isn't a solution what goes
      wrong when applying Newton's method and why does this happen.
    \end{exercise}

    It is possible to write the two initial value problems as one initial value
    problem.
    Let $u:[a,b]\to\R^{4n}$ satisfy the following equations,
    \begin{align}
      \label{equ_first_order_shooting_method_combined}
      \frac{d}{dx}u_1&=u_2,\\
      \frac{d}{dx}u_2&=f(x,u_1(x)),\\
      \frac{d}{dx}u_3&=u_4,\\
      \frac{d}{dx}u_4&=u_3\frac{\partial}{\partial u}f(x,u_1(x)),\\
      u_1(a)&=a_0,\\
      u_2(a)&=t,\\
      u_3(a)&=0,\\
      u_4(a)&=1.
    \end{align}

    \begin{exercise}
      Write down the system of first order initial boundary value problems
      that are suitable to use Newton's method to solve the boundary value
      problem below.
      Let $y:[-2,2] \to \R^n$ be such that
      \begin{align*}
        \left.\frac{d^2}{dx^2}y\right|_{x}a&=\exp(-x^2)
        y(-2)&=1,\\
        y(2)&=1.
      \end{align*}
    \end{exercise}

    Error results for the shooting method rely, of course, on the
    earlier error results for initial value problems and the chosen
    technique to solve the non-linear equation $\phi(t)=b$.

    \begin{theorem}[{\cite[Theorem 13.9]{suli2003introduction}}]
      Suppose that a numerical algorithm to solve the Equations
      \ref{equ_first_order_shooting_method_combined} and
      \[
        \frac{d}{dy}f(x,y)\neq 0,
      \] 
      for all $x$, gives the values
      $v_{i,j}(t)$ as the numerical approximation to the true values
      $u_i(x_j, t)$ are such that for all $i=1,2,3,4$,
      \[
        \max
          \left\{\abs{u_i(x_j, t) - v_{i,j}(t)}:j=1,\ldots, n\right\}
          \leq C(t)h^s,
      \]
      where $s, C(t)>0$ and $C(t)$ can depend on bounds on the derivatives
      of $y$ and $f(x,y)$ and $t$. Suppose also that Newton's method
      is used until
      \[
          \abs{v_{1,n}(t^{(k)})-b_0}<\epsilon,
      \]
      for some $\epsilon>0$. Then $v_{i,j}(t^{(k)})$ is an approximation
      of the boundary value problem which satisfies 
      \[
        \max\right\{\abs{y(x_j)-v_{1,j}(t^{(k)})}:j=1,\ldots,n\left\}
          \leq 2 C(t^{(k)})h^s + \epsilon.
      \]
    \end{theorem}
    \begin{proof}
      Suppose that the solution to the system of differential equations
      with $t=t^{(k)}$ is $u_1(x;t^{(k)}$, $i=1,2,3,4$ and that the
      corresponding numerical solution is $v_{i,j}(t^{(k)}$ then
      \[
          \abs{u_i(x_j, t) - v_{i,j}(t)} \leq C(t)h^s.
      \]
      Thus as $\abs{v_{1,n}(t^{(k)})-b_0}<\epsilon$,
      \begin{align*}
        \abs{u_1(b;t^{(k)})-b_0} 
          &\leq \abs{u_1(b;t^{(k)}-v_{i,j}(t^{(k)})} + 
            \abs{v_{i,j}(t^{(k)}-b_0}\\
          &\leq C(t^{(k)})h^s + \epsilon.
      \end{align*}
      Let $\eta(x;t)=y(x)-u_1(x;t)$ hence, by the intermediate value theorem
      \ben{inverse function theorem?}
      \[
        \frac{d^2}{dx^2}\eta = f(x,y(x)) - f(x,u_1(x;t)) 
          = \eta(x,t)\frac{d}{dy}f(x,\xi(x;t)),
      \]
      for some $\xi(x;t)$.
      Since $\frac{d}{dy}f$ is continuous and never equal to
      $0$ we know that it is of consistent sign.
      This implies that for all $x\in[a,b]$
      \[
        \abs{\eta(x;t)}\leq \abs{\eta(x;b)}.
      \]
      Hence
      \[
        \abs{y(x)-u_1(x;t^{(k)}}\leq C(t^{(k)})h^s+\epsilon
      \]
      and therefore
      \begin{align*}
        \abs{y(x_j)-v_{1,j}(t^{(k)})}
          &\leq\abs{y(x_j)-u_1(x_j;t^{(k)})}
            +\abs{u_1(x_j;t^{(k)})-v_{1,j}(t^{(k)})}
          &\leq 2C(t^{(k)})+\epsilon,
      \end{align*}
      as required.
    \end{proof}

    \begin{exercise}
      What goes wrong when $\frac{d}{dy}f(x,y)=0$?
    \end{exercise}

    \begin{exercise}[{\cite[Page 379]{suli2003introduction}}]
      Solve the boundary value problem,
      $y:[-1,1]\to\R$,
      \begin{align*}
        \frac{d^2}{dx^2}y&=y^2,\\
        y(-1)&=1,\\
        y(1)&=1.
      \end{align*}
    \end{exercise}

  \section{Reduction to a linear system of equations}
    
    For particular types of boundary value problems it is possible to
    reduce the system of differential equations to a system of
    linear equations. To do this finite differences are used.
    As before it is difficult to describe this method in general, so
    specific examples will be used.


  

    
    
         
