\chapter{Finding the eigenvalues and eigenvectors of matrices}

  Hopefully by now you know the importance of eigenvalues and eigenvectors
  in determining the behaviour of linear operators but also in determining
  the conditioning and numerical behaviour of algorithms.
  In this chapter we explore numerical algorithms for computing
  eigenvalues and eigenvectors. 

  \begin{example}
    Let $e_i\in\R$ be the standard basis for $\R^{16}$
    and let $A\in L(\R^{16},\R^{16})$ be the matrix so that
    $?A^i_i?=i + 1/3$ and $?A^i_j?=0$ otherwise.
    What are the eigenvalues of $A$? Numerically compute
    the eigenvalues of $A$? Why is there a difference?
    Apply the principles of rounding error analysis to determine
    why there is such a large difference.
  \end{example}

  \begin{definition}
    A matrix $A\in L(\C^n,\C^n)$ is tridiagonal
    with respect to the bases $e_i,f_i\in\C^n$ if
    $?A^i_j?=0$ if $\abs{i-j}\geq 2$.
  \end{definition}

  \begin{theorem}[{\cite[Theorem 5.7]{suli2003introduction}}]
    \label{thm_householder_triadiagonal}
    Let $A\in L(\R^n,\R^n)$ be symmetric then there
    exists an orthogonal matrix $Q$ so that
    $Q^TAQ$ is tridiagonal.
  \end{theorem}
  \begin{proof}
    Exercise. No really, you already know everything you need.
    The result follows from Theorem \ref{thm_QR_factorisation}
    because $A$ is symmetric.
  \end{proof}

  \begin{example}
    Let
    \[
      A=\begin{pmatrix}
        4 & 1 & 2 & 1 & 2  \\
        1 & 3 & 0 & -3 & 4 \\
        2 & 0 & 1 & 2 & 2 \\
        1 & -3 & 2 & 4 & 1 \\
        2 & 4 & 2 & 1 & 1
      \end{pmatrix}.
    \]
    By hand, and also using a computer, calculate the orthogonal transformation
    and the tridiagonal matrix given in Theorem 
    \ref{thm_householder_triadiagonal}.
  \end{example}

  \begin{proposition}[The Min-Max theorem]
    \label{prop_min_max}
    If $A\in L(\R^n,\R^n)$ is a symmetric linear operator
    and $\lambda_i\in \R$ are the eigenvalues of $A$ so that
    $\lambda_1\leq \lambda_2\leq \ldots\leq \lambda_n$ then
    \begin{align*}
      \lambda_k&=
        \min\left\{
          \max\left\{\frac{Ax\cdot x}{x\cdot x}:x\in U\setminus\{0\}\right\}
          :U\subset\R^n, \dim{U}=k
        \right\}\\
        &=\max\left\{
          \min\left\{\frac{Ax\cdot x}{x\cdot x}:x\in U\setminus\{0\}\right\}
          :U\subset\R^n, \dim{U}=n - k + 1
        \right\},
    \end{align*}
    so that for all $x\in\R^n$
    \[
      \lambda_1\leq\frac{Ax\cdot x}{x\cdot x}\leq\lambda_n
    \]
    and
    \begin{align*}
      \lambda_1
        &=\min\left\{\frac{Ax\cdot x}{x\cdot}:x\in\R^n\setminus\{0\}\right\}\\
      \lambda_n
        &=\max\left\{\frac{Ax\cdot x}{x\cdot}:x\in\R^n\setminus\{0\}\right\}.
    \end{align*}
  \end{proposition}
  \begin{proof}
    Exercise. The proof is reasonably direct. Pick an orthonormal basis
    for $\R^n$ made up of the eigenvectors of $A$. Any $k$ dimensional
    subspace must intersect the span of the last $n- k$ eigenvectors.
    The vector in this intersection can then be expressed as a linear sum
    of these last eigenvectors. Direct calculation shows that the
    appropriate limits are achieved through careful choices.
  \end{proof}

  \begin{theorem}[cauchy interlacing theorem]
    \label{thm_big_cauchy_interlacing_theorem}
    let $A\in L(\R^n,\R^n$ be symmetric and let $P\in L(\R^n,\R^n)$ be
    an orthogonal projection on to a subspace of dimension $m<n$.
    let $B=P^TAP$.
    if the eigenvalues of $A$ are $\alpha_1\leq\cdots\leq\alpha_n$
    and the eigenvalues of $B$ are $\beta_1\leq\cdots\leq\beta_m$ then
    for all $j=1,\ldots, m$ $\alpha_j\leq\beta_j\leq\alpha_{n-m+j}$.
  \end{theorem}
  \begin{proof}
    Exercise. This is a direct consequence of Proposition \ref{prop_min_max}.
    Suppose that $b_i\in\R^n$ be the eigenvector corresponding to
    $\beta_i$, $i=1,\ldots,m$, counting multiplicities.
    Let $S_j=\SPAN\{b_1,\ldots,b_j\}$. Proposition \ref{prop_min_max}
    implies that
    \begin{align*}
      \beta_j&=\max\left\{Bx\cdot x:x\in S_j,\norm{x}_2=1\right\}\\
        &=\max\left\{P^TAPx\cdot x:x\in S_j,\norm{x}_2=1\right\}\\
        &=\max\left\{APx\cdot Px:x\in S_j,\norm{x}_2=1\right\}\\
        &\geq\min\left\{
            \max\left\{Ax\cdot x:x\in U, \dim{U}=k, \norm{x}_2=1\right\}
          \right\}\\
        &=\alpha_j.
    \end{align*}
    Similarly taking $S_{m-j+1}=\SPAN\{b_j,\ldots,b_m\}$ Proposition
    \ref{prop_min_max}.
    \begin{align*}
      \beta_j&=\min\left\{Bx\cdot x: x\in S_{m-j+1}, \norm{x}_2=1\right\}\\
        &=\min\left\{P^TAPx\cdot x: x\in S_{m-j+1}, \norm{x}_2=1\right\}\\
        &=\min\left\{APx\cdot Px: x\in S_{m-j+1}, \norm{x}_2=1\right\}\\
        &\leq\alpha_{n-m_j}.
    \end{align*}
  \end{proof}

  \begin{exercise}
    Use the Proposition \ref{prop_min_max} to prove that
    \begin{align*}
        \min\left\{APx\cdot Px: x\in S_{m-j+1}, \norm{x}_2=1\right\}
        \leq\alpha_{n-m_j}.
    \end{align*}
  \end{exercise}

  \begin{definition}
    The roots, $x_1,\ldots, x_n$ of a polynomial separate the
    roots, $y_1,\ldots, y_m$ of a second polynomial if
    $n=m-1$ and for all $i$, $y_i< x_i< y_{i+1}$.
  \end{definition}
  
  \begin{theorem}[Also called Cauchy's interlacing theorem, see
    {\cite[Theorem 5.8]{suli2003introduction}}]
    Let $A\in L(\R^n,\R^n)$ be a tridiagonal matrix
    represented as
    \[
      A=\begin{pmatrix}
        a_1 & b_2 \\
        b_2 & a_2 & b_3 \\
            & b_3 & a_4 & b_4\\
            &     & \vdots & \vdots & \vdots \\
            &     &        & b_{n-1} &  a_{n-1} & b_n\\
            &     &        &         &  b_{n} & a_n\\
      \end{pmatrix},
    \]
    with $b_1\neq 0$ for all $i=1,\ldots,n$
    then
    the roots of the characteristic polynomial
    of $k$'th, $k=1,\ldots,n-1$, leading principal minor separates the
    roots of the characteristic polynomial of the
    $k+1$'\textsuperscript{th} leading characteristic polynomial.
  \end{theorem}
  \begin{proof}
    Let $\rho_0(\lambda)=1$.
    Let $\rho_k$ be the characteristic polynomial of the $k$\textsuperscript{th}
    leading principal minor. They satisfy the equations
    \begin{align*}
      \rho_1(\lambda) = a_1-\lambda,\\
      \rho_i(\lambda) = (a_i-\lambda)\rho_{i-1}(\lambda)-b_i^2\rho_{i-2}(\lambda).
    \end{align*}
    By Theorem \ref{thm_big_cauchy_interlacing_theorem}
    the roots of $\rho_k$ interlace the roots of $\rho_{k-1}$. To
    prove this result we must show that the polynomial have no roots in common.

    I will use a dissatisfying proof by induction.
    The base case is for $k=1$ in this case we have the following two
    characteristic polynomials to consider 
    \begin{align*}
      \rho_1(\lambda)&=a_1-\lambda,\\
      \rho_2(\lambda)&=(a_2-\lambda)(a_1-\lambda) + b_2^2.
    \end{align*}
    Thus it is clear that $a_1$ is not a root of $\rho_2$.

    Assume that for all $j<k$ we know that $\rho_j$ and $\rho_{j+1}$
    share no roots in common.
    Suppose that $\lambda$ is such that $\rho_k(\lambda)=0$ and
    $\rho_{k-1}(\lambda)=0$.
    Then from the defining equation for $\rho_{k}$,
    $\rho_k(\lambda)=0$ implies that 
    $\rho_{k-2}(\lambda)=0$. But then $\rho_{k-2}(\lambda)=0$ 
    and $\rho_{k-1}(\lambda)=0$ and this violates the inductive assumption.
    The result is proven.
  \end{proof}

  See \cite[Theorem 5.8]{suli2003introduction} for a much more satisfying
  inductive prove. 

  \begin{exercise}
    Prove the recurrence relation for the characteristic polynomials for
    the leading principal minors.
  \end{exercise}

  \begin{theorem}[{\cite[The Sturm sequence property]}]
    Let $A\in L(\R^n,\R^n)$ be a tridiagonal matrix
    and for each $i=1,\ldots, n$ let $\rho_i(\lambda)$ be the characteristic
    polynomial for the $i$\textsuperscript{th} leading principal
    minor. If $\theta\in \R$ then 
    the number of agreements in sign, consecutively, of the sequence
    $\rho_i(\theta)$, $i=1,\ldots,n$ is the number of eigenvalues of
    $T$ greater than $\theta$.
  \end{theorem}

  \ben{Look up Sturm sequence in other numerical texts. Need an error analysis.}
