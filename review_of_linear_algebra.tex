\chapter{Linear algebra overview}
  
  It is my expectation that you know this stuff already. I introduce
  Einstein summation and some common conventions in advanced mathematics
  for handling indices. I am emphasising these particular formula and
  this way of writing formula because
  they tell you how to do the computation in a computer.

  A real $n\times m$ matrix, $A$, is a linear map from a $\R^n$ to $\R^m$.
  The vector space of all $n\times m$ real matrices is denoted, in this
  class, by $L(\R^n, \R^m)$. If $?e_i?$, $i=1,\ldots, n$,
  and $?u_j?$, $j=1,\ldots, m$ are bases for $\R^n$ and $\R^m$ respectively
  then we can define $?A^j_i?\in\R$ as the $nm$ numbers so that
  $Ae_i=\sum_{j=1}^m?A^j_i?u_j$. 
  In this class
  I will write 
  $?A^j_iu_j?$ rather than $\sum_{j=1}^m?A^j_i?u_j$. Sums are implied over
  repeated indices. This has the advantage of removing the $\sum$ symbol, which
  is large, culturing and doesn't convey much. It has the disadvantage
  that you need to remember the range for the index $j$. This means that 
  the index $j$ ``carries'' more information and so is more important that
  in notation that uses the sum. Overall the advantage outweighs the
  disadvantage, but occasionally we'll need to revert back to using
  sums. I'll come back to the tiny $\cdot$ in $?A^j_i?$ in a moment.

  By a matrix I mean the
  numbers $(?A^j_i?)_{i,j}$ organised into a grid.
  Let $v\in\R^n$ then $Av=w\in\R^m$ and if $v=v^ie_i$ then
  \begin{align*}
    Av&=Av^ie_i\\
      &=v^i(Ae_i) &&\text{by linearity}\\
      &=v^i(?A^j_i?u_j)\\
      &=(?A^j_i?v^i)u_j.
  \end{align*}
  Thus if $w=w^ju_j$ then $w^j=?A^j_i?v^i$.

  The (Euclidean) inner product of two vectors $v,w\in\R^n$ is
  written
  $v\cdot w$ or $(v,w)$. If $v=v^ie_i$ and $w=w^ie_i$ then
  $(v,w)=v^iw^j(e_i,e_j)$, $i,j=1,\ldots, n$. If the $e_i$'s are
  orthonormal then $(e_i,e_j)=\delta_{ij}$ so
  $(v,w)=\sum_{i}v^iw^i$.
  The Kronecker delta, $\delta_{ij}$, $i,j=1\ldots, n$, 
  is a symbol representing the number
  $0$, or $1$  defined by
  \[
    \delta_{ij}=\left\{\begin{aligned}
      & 0 && i\neq j\\
      & 1 && i = j.
    \end{aligned}\right.
  \]
  Hence in the case when the $e_i$'s are orthonormal
  we can write $(u,v)=v^iw^j\delta_{ij}$.

  The transpose of $A\in L(\R^n,\R^m)$ is the
  linear map $A^T\in L(\R^m,\R^n)$ defined by
  $(Au,v)_m=(u, A^Tv)_n$ for all $u\in\R^n$ and
  $v\in\R^m$.
  Let $e_i$, $i=1,\ldots,n$ and
  $u_j$, $j=1,\ldots,m$ be orthonormal basis
  of $\R^n$ and $\R^m$ respectively.
  The definition of the transpose implies that
  $?A^j_i?=(Ae_i,u_j)=(e_i,A^\top u_j)={A^\top}?{}^i_j?$.
  Note that the indices have changed.

  \begin{exercise}
    Prove that $(AB)^\top = B^\top A^\top$.
  \end{exercise}
  
  It is very important to note the little dot $\cdot$ in the
  notation for the numbers $?A^j_i?$ and $?{A}^i_j?$.
  That dot indicates that the $i$
  occurs after the $j$ or vice versa. The order has meaning.
  In particular we can now say, with clear notation,
  that the components of the transpose of $A$ in
  the basis given above are
  $?A^j_i?$ and the components of $A^\top$ in the
  same bases are $?A^i_j?$.
  That is $A^\top u_j=\sum_{i}?A^j_i?e_i$
  while $Ae_i=?A^j_i?u_j$. This is a subtle distinction that
  won't come up often --- but it is important to be aware of.
  
  The Levi-Civita symbol (also called the permutation symbol,
  antisymmetric symbol or alternating symbol) is like the Kronecker delta
  but
  it takes the values $-1,0,1$ and is defined over an
  arbitrary number of indices. If we have $n$ indices
  the Levi-Civita is written
  $\epsilon_{i_1i_2\cdots i_n}$. It is $0$ if
  $i_j=i_k$ for any $j,k=1\ldots, n$. The value of
  $\epsilon_{12\dots,n}$ is $1$ and if $\sigma$ is the permutation
  so that $\sigma(j)=i_j$ then
  $\epsilon_{i_1i_2\cdot i_n}=\sign{\sigma}$.
  Both the Kronecker delta and Levi-Civita symbol have ``upper indices''
  versions, $\delta^{ij},\epsilon^{i_1i_2\cdot i_n}$ as well as
  ``mixed'' versions
  $\delta^{i}_j,\epsilon^{i_1i_2\cdot i_n}$ as well as

  The determinant is a function $\det:L(\R^n,\R^n)\to\R$
  defined by 
  \begin{align*}
    \det(A)&=\epsilon^{i_1i_2\cdot i_n}?A^1_{i_1}??A^2_{i_1}?\cdot?A^n_{i_1}?\\
           &=\epsilon_{j_1j_2\cdot j_n}
             \epsilon^{i_1i_2\cdot i_n}
             ?A^{j_1}_{i_1}??A^{j_2}_{i_1}?\cdot?A^{j_n}_{i_1}?.
  \end{align*}
  The trace is also a function $\tr:L(\R^n,\R^n)\to \R$ given by
  $\tr(A)=\sum_{i=1}^n ?A^i_i?=?A^i_i?$, where I have included
  the $\sum$ for clarity only. You should prove that both the
  determinant and the trace are independent of the choice
  of basis.

  The $k$\textsuperscript{th} leading principal minor of
  a matrix $A\in L(\R^n,\R^m)$, $k\leq\min\{n,m\}$ 
  is the
  sub-matrix of $A$ consisting of the first $k$ rows and columns.

  \begin{theorem}[{\citep[Theorem 7.2]{atkinson2008introduction}}]
    Let $A\in L(\C^n,\C^n)$ then the following are equivalent.
    \begin{enumerate}
      \item The equation $Ax=b$ has a unique solution $x\in\C^n$ 
        for all $b\in\C^n$.
      \item The equation $Ax=b$ has a solution $x\in\C^n$ 
        for all $b\in\C^n$.
      \item The equation $Ax=0$ has the unique solution $x=0$.
      \item The inverse to $A$ exists.
      \item The determinant of $A$ is nonzero.
      \item The rank of $A$ is $n$.
    \end{enumerate}
  \end{theorem}
  \begin{proof}
    Exercise.
  \end{proof}

  \section{Eigenvalues and eigenvectors}

    If $A\in L(\R^n,\R^n)$ then an eigenvector for $A$ is a vector
    $v\in\R^n$ so that there exists $\lambda\in\R$ so that
    $Av=\lambda v$. The number $\lambda$ is called an eigenvalue.
    A number $\lambda$ is an eigenvector if and only if
    $\det(A-\lambda I)=0$. A linear map, $A\in L(\R^n,\R^n)$ has an inverse if
    and only if $\det(A)\neq 0$.

    \begin{exercise}
      How would you check if $\det(A)\neq0$ in a computer? What could go wrong?
    \end{exercise}

    \begin{exercise}
      What are the eigenvalues and eigenvectors of
      the following matrices?
      \begin{enumerate}
        \item \[
            \begin{pmatrix}
              2 & 1 & 0\\
              1 & 3 & 1\\
              0 & 1 & 2
            \end{pmatrix},
          \]
        \item \[
            \begin{pmatrix}
              1 & 1 & 0\\
              0 & 1 & 1\\
              0 & 0 & 1
            \end{pmatrix},
          \]
        \item \[
            \begin{pmatrix}
              1 & 4 \\
              1 & 1
            \end{pmatrix},\text{ and }
          \]
        \item \[
            \begin{pmatrix}
              1 & 1 \\
              -1 & 3
            \end{pmatrix}.
          \]
      \end{enumerate}
    \end{exercise}

    \begin{exercise}
      Let $x\in\R^n$ so that $A=xx^\top\in L(\R^n,\R^n)$ with
      two eigenvalues, one of multiplicity $n-1$ the other
      of multiplicity of $1$.
    \end{exercise}

    \begin{definition}
      A linear operator $A\in L(\R^n,\R^n)$ is symmetric if
      $A=A^T$. If $e_i\in\R^n$ is a basis for
      $\R^n$ then $A$ is symmetric if and only if
      $?A^i_j?=?A^j_i?$.
    \end{definition}

    \begin{exercise}
      Prove that $A=A^T$ if and only if
      $?A^i_j?=?A^j_i?$.
    \end{exercise}

    The theory of eigenvalues for symmetric matrices is rather nice.
    A symmetric matrix with dimension $n$ will have $n$ linearly
    independent eigenvectors each with a real
    eigenvalue. The polynomial equation $\det(A-\lambda I)=0$ in $\lambda$
    has $n$ real roots, counted with multiplicity,
    which are the eigenvalues of $A$. It is
    called the characteristic polynomial of $A$. Two eigenvectors
    are orthogonal if and only if their eigenvalues are different. 
    The multiplicity of an eigenvalue as a root of the characteristic
    polynomial is called its \emph{algebraic multiplicity}.
    The dimension of the null space of the linear operator
    $A-\lambda I$, when $\lambda$ is an eigenvalue, is called the
    \emph{geometric multiplicity} of the eigenvalue.

    \begin{exercise}
      Prove that if $A$ is a symmetric matrix then
      the algebraic multiplicity and geometric multiplicity of an
      eigenvalue is the same.
    \end{exercise}

    The eigenvalues of a symmetric linear operator have equal
    algebraic and geometric multiplicities, hence the multiplicity of
    an eigenvalue of a symmetric linear operator is either the
    algebraic or the geometric multiplicity.
    If an eigenvalue has multiplicity $m$ then there are $m$ orthogonal
    eigenvectors corresponding to $m$.
    If the columns of $X$ are the normalised eigenvectors of $A$
    then $X^TAX$ is a diagonal matrix whose entries are 
    eigenvalues.

    \begin{exercise}
      Prove that $X$ is an orthogonal matrix.
    \end{exercise}

    The set of all eigenvectors of a symmetric linear operator of dimension
    $n$ span $\R^n$. The trace of a symmetric linear operator is the
    sum of the eigenvalues.

    \begin{definition}
      A real matrix $A\in L(\R^n,\R^m)$ is orthogonal if
      $(Ax, Ax)=(x,x)$. 
      A complex matrix $A\in L(\C^n,\C^m)$ is unitary if
      $\overline{A^T}A=1$. 
      A common short hand
      notation for $\overline{A^\top}$ is $A^*$.
    \end{definition}

    \begin{exercise}
      Let $A\in L(\R^n,\R^m)$ show that
      $A^\top A=1$ if and only if $A$ is orthogonal.
    \end{exercise}

    \begin{exercise}
      Let $A\in L(\R^n,\R^m)$ show that
      $A^\top A=1$ if and only if $A$ is orthogonal.
    \end{exercise}

    \begin{exercise}
      Let $A\in L(\C^n,\C^m)$ show that
      $A$ unitary implies that $\det(A)\neq 0$.
    \end{exercise}
    
    \begin{exercise}
      Write down a formula for the components of
      $\overline{A^\top}$ in terms of the components of $A$.
      Check that
      $\overline{A^\top}=\overline{A}^\top$.
    \end{exercise}
    
  
  \subsection{Norms}

    We need a way to measure the size of $x$'s, $b$'s and $A$'s.

    \begin{definition}
      A norm is a function $\norm{\cdot}:\C^n\to \R$ so that
      \begin{enumerate}
        \item $\norm{x}\geq 0$ and if $\norm{x}=0$ then $x=0$,
        \item $\norm{rx}=\abs{r}\norm{x}$,
        \item $\norm{x + y}\leq \norm{x}+\norm{y}$.
      \end{enumerate}
    \end{definition}

    Norms play the same role as the absolute value: it tells us about size.

    \begin{example}
      \begin{enumerate}
        \item If $n=1$ then the absolute value is a norm,
        \item The function $\norm{x}_2=\sqrt{\sum_{i=1}^n x_i^2}$ is called the 
          Euclidean norm (or $2$-norm),
        \item The square root of the dot product is the Euclidean norm,
        \item The function $\norm{x}=\sum_{i=1}^n\abs{x_i}$ is called
          the taxi cab norm (because taxi's only exist in New York apparently),
        \item The norm
          \[
            \norm{x}_p=\left(\sum_{i=1}^n\abs{x_i}^{p}\right)^{\frac{1}{p}}
          \]
          is called the $p$-norm.
        \item The infinity norm on $\C^n$ is
          \[
            norm{x}_\infty=\max\{\abs{x_i}:i=1,\ldots,n\}.
          \]
      \end{enumerate}
    \end{example}

    \begin{exercise}[{\citep[Problem 7.21]{atkinson2008introduction}}]
      Prove the following for $x\in\C^n$.
      \begin{enumerate}
        \item $\norm{x}_\infty\leq\norm{x}_1\leq n\norm{x}_\infty$.
        \item $\norm{x}_\infty\leq\norm{x}_2\leq\sqrt{n}\norm{x}_{\infty}$.
        \item $\norm{x}_2\leq\norm{x}_1\leq \sqrt{n}\norm{x}_2$.
      \end{enumerate}
    \end{exercise}

    \begin{lemma}[{\citep[Exercise 7.23]{atkinson2008introduction}}]
      For all $x\in\C^n$
      \[
        \lim_{p\to\infty}\norm{x}_p=
        \lim_{p\to\infty}\left(\sum_{j=1}^n\abs{x_i}^p\right)^{1/p}=
        \max\{\abs{x_i}:i=1,\ldots,n\}=\norm{x}_\infty.
      \]
    \end{lemma}

    \begin{definition}
      A matrix norm (or just norm) is a function $\norm{\cdot}:M(\R, n)\to \R$ so that
      \begin{enumerate}
        \item $\norm{A}\geq 0$ and if $\norm{A}=0$ then $A=0$,
        \item $\norm{rA}=\abs{r}\norm{A}$,
        \item $\norm{A + B}\leq \norm{A}+\norm{B}$,
      \end{enumerate}
    \end{definition}

    \begin{example}
      \begin{enumerate}
        \item The $p$ operator norm is
          $\norm{A}_p=\sup\left\{\frac{\norm{Ax}_p}{\norm{x}_p}:x\in\R^n, x\neq 0\right\}$.
        \item The Frobenius or Hilbert-Schmidt norm is $\norm{A}_F=\tr{\sqrt{A^TA}}$.
        \item The entrywise $p$ norm is
          $\norm{A}_p=\left(\sum_{i,j=1}^n\abs{a_{ij}}^p\right)^\frac{1}{p}$. This
          is the vector $p$ norm of $A$, when $A$ is written as a vector.
      \end{enumerate}
    \end{example}

    \begin{definition}
      A matrix norm is sub-multiplicative if
      $\norm{AB}\leq \norm{A}\norm{B}$.
    \end{definition}

    \begin{exercise}
      \begin{enumerate}
        \item What is the formula for $\norm{A}_1$?
        \item What is the formula for $\norm{A}_\infty$?
        \item Show that $\norm{A}_2\leq\norm{A}_F$.
        \item Show that the entrywise matrix $p$ norm is the $p$ norm of
          a vector.
      \end{enumerate}
    \end{exercise}

    \begin{exercise}
      For all $x\in\C^n$ and $A,B\in L(\C^n,\C^m)$
      show that 
      \begin{enumerate}
        \item $\norm{Ax}_2\leq\norm{A}_F\norm{x}_2$,
        \item $\norm{AB}_F\leq \norm{A}_F\norm{B}_F$, and
        \item $\frac{1}{\sqrt{n}}\norm{A}_F\leq \norm{A}_2\leq\norm{A}_F$.
      \end{enumerate}
    \end{exercise}

    \begin{lemma}\label{lemma_norm_invariant_unitary}
      If $A$ is a matrix and $U$ is unitary then
      $\norm{AU}=\norm{UA}=\norm{A}$.
    \end{lemma}
    \begin{exercise}
    \end{exercise}

    \begin{exercise}
      Why won't Lemma \ref{lemma_norm_invariant_unitary} hold
      in a computer? How much error will there be in the
      calculation of $\norm{UA}$?
    \end{exercise}

  \ben{Gaussian elimination with pivoting}
