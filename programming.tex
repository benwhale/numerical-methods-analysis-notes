\chapter{Programming}

  It is expected that you either know how to program or that you have committed
  to learning how to program when enrolling in this class. 
  This chapter gives a very basic guide to programming - it is up to you to
  teach yourself beyond the basics.

  Figure \ref{figure_tiobe_index} shows the Tiobe index for a variety of
  programming languages. The index is designed to be interpreted as a measure
  of popularity. Java is first followed closely by C. A long way behind
  Java and C are C++ and Python which are almost as popular as each other.
  There is then a gap to the remaining languages.

  \begin{figure}[!htb]
    \centering
    \includegraphics[width=\linewidth]{Tiobe_index.png}
    \caption{The Tiobe programming language popularity index from 2002 to 2018}
    \label{figure_tiobe_index}
  \end{figure}

  It's a little facetious to claim that you should choose a language based on 
  popularity... but that is what I'm going to do. Java is a pain to learn because
  of all the boiler plate and we will not need the complexity of C++ in this
  class. As a consequence I will be mostly writing in C and Python. I expect
  you to be able to write one of C, Python or (due to popular demand) Matlab 
  (which is just C with extra steps). I expect you to be able to read
  C and Python. I will on occasion also use SageMath (which is Python with
  extra steps).

  You will only need to write basic programs, so the amount of learning
  required is small. You will need to learn how to:
  \begin{enumerate}
    \item write code, compile it and execute it,
    \item evaluate mathematical operations, including the ``standard'' math
      functions ($\exp, \log, \sin, \cos,$ and so on),
    \item construct arrays and perform operations on / with them,
    \item matrix multiplication,
    \item print data to the screen.
  \end{enumerate}

  It would be very good practice for you to also learn (and use) how to
  \begin{enumerate}
    \item write and call a function,
    \item pass data (including arrays) to a function,
    \item return data from a function.
  \end{enumerate}

  \section{An overview of programming languages}

    The following is an attempt to give an overview of what a programming language
    is without being very technical (and it is possible to be very technical,
    e.g.\ look up Backus-Naur Form). Listings \ref{lst_python_addition}
    and \ref{lst_c_addition} present two basic programs, one written in
    python the other in C.

    \begin{minipage}[t]{0.45\textwidth}
      \captionof{listing}{A simple Python program}
      \label{lst_python_addition}
      \begin{python}
        x = 10
        y = 20

        print x + y, "Done"
      \end{python}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{0.45\textwidth}
      \captionof{listing}{A simple C program}
      \label{lst_c_addition}
      \begin{cmint}
        #include <stdio.h> 

        int main() {
          int x = 10;
          int y = 10;
          printf('%f', x + y);
        }
      \end{cmint}
      \vspace{2ex}
    \end{minipage}

    Each programming language will have all of the following, which
    constitute the alphabet for a programming language.
    \begin{description}
      \item[identifiers] Symbols that 
        represent a variable, a function, an object, a file, and so on.
        In the examples above \pythoninline|x|, \pythoninline|y|,
        \cinline|x|, \cinline|y|, \cinline|<stdio.h>|, \cinline|printf| 
        are identifiers.
      \item[keywords] Symbols that are reserved for use to communicate
        an action to the compiler.
        In the examples above \pythoninline|print|,
        \cinline|int|, \cinline|main|, \cinline{#include}, 
        \cinline|printf| are keywords.
      \item[operator] Symbols that perform a function. Often the function has
        a reserved meaning or reserved behaviour.
        In the examples above \pythoninline|=|, \pythoninline|+|
        \cinline|=|, \cinline|+|, are operators.
      \item[separators] Symbols that separate logical statements in the language.
        In the examples above both python and c use spaces and commas, 
        python also uses the end of line and cartridge return characters, while
        c uses round and curly brackets and semi-colons.
      \item[literals] Symbols that have special fixed predefined meaning.
        Most programming languages use alphanumeric characters (based on the
        encoding of the file), in the examples above we have $10$, $20$, 
        \pythoninline|"Done"| and \cinline|'%f'|.
      \item[comments] Symbols that mean: ``anything after this is not part of the
        code''.
        In the examples above \pythoninline|#| and
        \cinline|//| are comment symbols. In latex the comment symbol is 
        \latexinline|%|.
    \end{description}

    There are rules about how the alphabet can be used. These rules are the
    grammar of the programming language. The grammars for python and c are
    very simple. Because the grammars are designed for a computer, rather than
    a human, they are difficult to comprehend in the abstract. 
    Take a look at the ``full grammar specification'' for python.
    You can find it at
    \url{https://docs.python.org/3/reference/grammar.html}
    It is best therefore to learn the grammar for the 
    programming language as you go and as you use it. Just like when learning
    a foreign spoken language.

    There are, however, a few common elements to most procedural languages.
    \begin{description}
      \item[basic datatypes]
        Usually this is limited to various types of numbers and strings.
      \item[basic operators]
        The standard numerical operations, string concatenation, often operators
        for modular arithmetic. 
      \item[variable assignment] 
        A variable is an identifier which stands in place for a particular value.
        If \pythoninline|x = 10| then instead of writing \pythoninline|10|
        everywhere I could write \pythoninline|x|. This is most useful when
        some value is not fixed when writing (or strictly compiling) the code
        but rather is the result of user input.
      \item[control statements]
        Keywords that allow you to control which of several statements are executed
        and how often they are executed. The most common control statements
        are \pythoninline{if}, \pythoninline{for} and \pythoninline{return} statements. 
        You will also see \pythoninline{else}, \pythoninline{elif}, 
        \pythoninline{while}, \pythoninline{try}, \pythoninline{except}, 
        \pythoninline{pass}.
        C has some special control statements that you will also see in
        older languages \cinline{switch}, \cinline{case}, 
        \cinline{?} and \cinline{:}. Though
        \cinline{?} is called the ternary operator it is also a control statement.
      \item[data structures]
        It is not useful to use a different variable name for each value. What if
        you don't know how many values you will need to work with? Data structures
        are ways to keep track of variable amounts of data. Interpreted languages,
        or even fancy compiled languages like Objective-C and Swift, hide
        as much detail about data structures as possible. Older and most compiled
        languages have lots and lots of subtly different data structures.
        The data structure needs to match the way you intend to use the data as
        the choice of data structure can have a big effect on the speed of 
        execution. Examples of data structures are, lists, arrays, vectors,
        linked lists, trees, dictionaries, maps, hash maps, and inumerable
        variations on these.
      \item[input and output]
        Any programming language worth anything needs to be able to read 
        information provided by the user and to store computed data for later
        use. 
      \item[data organisation]
        Sometimes multiple data structures are needed to manage information related
        to one conceptual thing. Most languages provide ways to manage multiple
        data structures like this. In python it is objects, in c it is structs.
      \item[code organisation]
        When a program runs it executes lines of code in order. Most languages
        provide ways to collect pieces of code that are related to each other
        together. In python these are called namespaces which take the form
        of files and objects. C is a weak on code organisation. Essentially the
        only way to organise code is to put similar code in one file.
      \item[``standard'' libraries]
        Most languages come with a substantial amount of pre-written code for you
        to use. This code is not strictly speaking part of the grammar, but you
        can think of the pre-written code as part of the colloquial expressions
        and cultural use of the language. Python incorporates a large amount
        of pre-written libraries. C has ``the standard library'' which is
        more limited. There is a collection of C and C++ code called ``The
        Boost Libraries'' that contain substantially more code.
    \end{description}

    Programming languages often have a philosophy about how programming
    should be done. This philosophy forms part of a wider mental map
    about how data relates to code. Sometimes it can be helpful to understand
    this mental map and the associated philosophy to learn the language.

    If you are trying to make a decision about what language to learn: choose
    python. Look up the python 3 tutorial 
    (\url{https://docs.python.org/3/tutorial/}) and start reading.

  \section{How to write code}\label{section_how_to_write_code}
    
    The best code is working code, followed closely by readable code. 
    
    Read the
    documentation. My advice is that videos 
    \begin{enumerate}
      \item are an inefficient way to learn,
      \item often too short to explain detail, and
      \item do not exist for every problem you will have.
    \end{enumerate}
    Don't read someone else's explanation of the documentation. Just go
    read the documentation. Yes it's difficult and full of detail. Yes you need
    to learn that detail. Don't be scared of hard things, you are at university. If
    you balk at the idea of reading documentation then god help us all when you're
    running a multi-billion dollar company, operating on my dodgy hip or figuring
    out how to save Koala's from extinction. Someone has already done the really
    hard work of writing the documentation, all you need to do is read it.

    The following is taken from PEP20 -- The Zen of Python. It describes
    what code should be and, I think, provides valuable guidance for writing code
    in any language.
    \begin{quote}
      Beautiful is better than ugly.

      Explicit is better than implicit.

      Simple is better than complex.

      Complex is better than complicated.

      Flat is better than nested.

      Sparse is better than dense.

      Readability counts.

      Special cases aren't special enough to break the rules.

      Although practicality beats purity.

      Errors should never pass silently.

      Unless explicitly silenced.

      In the face of ambiguity, refuse the temptation to guess.

      There should be one -- and preferably only one -- obvious way to do it.

      Although that way may not be obvious at first unless you're Dutch.

      Now is better than never.

      Although never is often better than \emph{right} now.

      If the implementation is hard to explain, it's a bad idea.

      If the implementation is easy to explain, it may be a good idea.

      Namespaces are one honking great idea -- let's do more of those!
    \end{quote}

    To actually write code you will need a text editor. \emph{Definitely} not
    a word processor. Just something that allows to you edit text. I use vim.
    I do not suggest that you use vim (unless you are a masochist in search of
    nirvana). Most languages have text editors that are designed for that language.
    Read the language's documentation to look this up.

    Once you've got some code in a file you either need to compile it and then
    run it or just compile it. In python the command to compile and run is
    \bashinline|python <name of file>|. C is a little more complicated but for
    our purposes the commands will be something like 
    \bashinline|gcc <name of file>| followed by \bashinline|./<name of output file>|.
    Guess were you can go to actually find out how to do this... 
    that's right the documentation.

  \section{Debugging}
    
    The key to debugging is to understand that computers are stupid, they just do
    the same thing over and over and over again. Given \emph{exactly} the same
    input they will produce \emph{exactly} the same output. You can exploit this
    to your advantage: if your code has an error then you can repeatedly run the
    code over and over and over again with \emph{exactly} the same input 
    with the sure knowledge that the same error will reoccur in the same way
    each time. By inspecting the programs state each time you rerun the code
    you can eventually pin point where the error is.

    If it appears that the error changes each time you run your code then
    you are not providing \emph{exactly} the same input. What are you doing
    differently? Computer's can't do things differently, that's not how they
    work - that's not how any computer works. 

    \begin{center}
       \large\emph{The eleven principals of 
       debugging\\ by Nick Parlante of Stanford University}
    \end{center}
    \begin{quote}
      \begin{enumerate}
        \item Intuition and hunches are great—you just have to test them out. When
          a hunch and a fact collide, the fact wins. That's life in the city.

        \item Don’t look for complex explanations. Even the simplest omission
          or typo can lead to very weird behavior.  Everyone is capable producing
          extremely simple and obvious errors from time to time. Look at code
          critically—don’t just sweep your eye over that series of simple
          statements assuming that they are too simple to be wrong.

        \item The clue to what is wrong in your code is in the values of your
          variables and the flow of control. Try to see what the facts are
          pointing to. The computer is not trying to mislead you. Work from the
          facts.

        \item Be systematic and persistent. Don’t panic. The bug is not moving
          around in your code, trying to trick or evade you. It is just sitting
          in one place, doing the wrong thing in the same way every time.

        \item If you code was working a minute ago, but now it doesn’t—what was
          the last thing you changed? This incredibly reliable rule of thumb is
          the reason your section leader told you to test your code as you go
          rather than all at once.

        \item Do not change your code haphazardly trying to track down a bug.
          This is sort of like a scientist who changes more than one variable in
          an experiment at a time. It makes the observed behavior much more
          difficult to interpret, and you tend to introduce new bugs.

        \item If you find some wrong code that does not seem to be related to
          the bug you were tracking, fix the wrong code anyway. Many times the
          wrong code was related to or obscured the bug in a way you had not
          imagined.

        \item You should be able to explain in Sherlock Holmes style the series
          of facts, tests, and deductions that led you to find a bug.
          Alternately, if you have a bug but can’t pinpoint it, then you should
          be able to give an argument to a critical third party detailing why
          each one of your functions cannot contain the bug. One of these
          arguments will contain a flaw since one of your functions does in fact
          contain a bug. Trying to construct the arguments may help you to see
          the flaw.

        \item Be critical of your beliefs about your code. It’s almost
          impossible to see a bug in a function when your instinct is that the
          function is innocent. Only when the facts have proven without question
          that the function is not the source of the problem should you assume it
          to be correct.

        \item Although you need to be systematic, there is still an enormous
          amount of room for beliefs, hunches, guesses, etc. Use your intuition
          about where the bug probably is to direct the order that you check
          things in your systematic search. Check the functions you suspect the
          most first. Good instincts will come with experience.

        \item Debugging depends on an objective and reasoned approach. It
          depends on overall perspective and understanding of the workings of
          your code. Debugging code is more mentally demanding than writing code.
          The longer you try to track down a bug without success, the less
          perspective you tend to have.  Realize when you have lost the
          perspective on your code to debug. Take a break. Get some sleep. You
          cannot debug when you are not seeing things clearly. Many times a
          programmer can spend hours late at night hunting for a bug only to
          finally give up at 4:00A.M. The next day, they find the bug in 10
          minutes.  What allowed them to find the bug the next day so quickly?
          Maybe they just needed some sleep and time for perspective. Or maybe
          their subconscious figured it out while they were asleep. In any case,
          the “go do something else for a while, come back, and find the bug
          immediately” scenario happens too often to be an accident.
      \end{enumerate}
    \end{quote}

  \section{A quick overview of C}
    
    The language C is an elegant language designed for a more civilised age.
    Modern languages provide ``semantic views'' of memory. By this I mean
    that the data you manipulate is not what is actually stored in
    RAM. For C the data you see is the data stored in RAM. This difference
    means that C is very simple. Of course this means that more C code is
    usually required to write a program than for the same program written
    in a different language (except perhaps Java). 
    Listings
    \ref{listing_c_fork_bomb} and \ref{listing_c_add_two_numbers} given example
    programs. C doesn't assume much beyond ``ways to manipulate and interpret
    bits in memory'' that's the reason why C code is usually significantly longer
    and more complicated than Python.

    \begin{listing}
    \end{listing}
    \begin{minipage}[t]{0.45\textwidth}
      \captionof{listing}{This program that will crash your computer.
        This is called a Fork Bomb.}
      \label{listing_c_fork_bomb}
      \begin{cmint}
        #include <sys/types.h>
        #include <unistd.h>

        int main()
        {
            while(1) {
              fork();
            }
            return 0;
        }
      \end{cmint}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{0.45\textwidth}
      \captionof{listing}{C code to add two number together and write them out.}
      \label{listing_c_add_two_numbers}
      \begin{cmint}
        #include <stdio.h>

        int main()
        {
          double x = 1;
          double y = 1;
          double z = x + y;
          print("%f", z);
        }
      \end{cmint}
      \vspace{2ex}
    \end{minipage}

    Because C is simple it needs to be told everything. Because everything is
    a portion of memory you need to tell C how to interpret the bits
    it finds in memory locations. Thus every variable needs to be preceded
    by a \emph{type} that tells C how much memory this variable will need.
    The equals sign writes values to memory and the use of the variable
    results in the memory being used at that point in the computation.

    The \cinline{#include} commands tell C to import code from another
    file. Thus the \cinline{<stdio.h>} is referring to a file located
    somewhere on your machine.
    The \cinline|int main() {| is a function declaration. The \cinline{main()}
    function is called when the program is run. It returns an
    \cinline{int}, that is a piece of memory that contains bits representing
    an integer. In the first example the return value $0$ is explicitly given.
    The command \cinline{print()} is also a function, one whose
    code is contained in or referenced by the \cinline{stdio.h} file.
    The statement \cinline{while} is a while loop. This tells the program
    to execute the ``contents'' of the loop (everything inside the brackets)
    when the code in the brackets is true. Then, if the code in the brackets
    still evaluates to true do it all again!

    Thus the first program calls a function \cinline{fork()} this is a function
    that tells the operating system to put aside some memory for a new program.
    Because the loop runs for ever, eventually there will be no memory left and
    your computer will crash.

    The second program adds \cinline{x} and \cinline{y} together and calls
    the \cinline{print()} function. The function \cinline{print()} needs
    to know how to format the string representation of the data you give it.
    The string \cinline{"%f"} 
    tells \cinline{print()} to format \cinline{z} as a floating point number.

    There is great documentation for C online. The best way to learn is to read
    documentation and then write code. 
    I recommend \url{https://en.cppreference.com/w/c}
    see \url{https://en.cppreference.com/w/c/language} for basics.
    For simple examples to write go to 
    \url{https://adriann.github.io/programming_problems.html}

  \section{Python}

    I assume that you have already done a course on Python.
    Documentation is at \url{https://docs.python.org/3/}.
    Start with the tutorial if you need to
    \url{https://docs.python.org/3/tutorial/interpreter.html}.
    Listings \ref{listing_python_fork_bomb}
    and \ref{listing_python_string_concatonation} are two 
    programs similar to 
    Listings
    \ref{listing_c_fork_bomb} and \ref{listing_c_add_two_numbers}.
    You should compare them.

    \begin{minipage}[t]{0.45\textwidth}
      \captionof{listing}{A Python fork bomb. Running this program \emph{might}
        crash your machine.}
      \label{listing_python_fork_bomb}
      \begin{python}
      import os
      while True:
          os.fork()
      \end{python}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{0.45\textwidth}
      \captionof{listing}{A program to demonstrate string concatenation.}
      \label{listing_python_string_concatonation}
      \begin{cmint}
        x = "Hello"
        y = "World"
        z = x + y
        print(z)
      \end{cmint}
      \vspace{2ex}
    \end{minipage}

    The philosophy of Python is contained in PEP20, see
    Section \ref{section_how_to_write_code}. In short, Python aims to be a 
    language that reads and writes well. You can see this reflected
    in the choices made in PEP8 
    \url{https://www.python.org/dev/peps/pep-0008/}, which describes the
    style of Python coding in a similar way to style guides for English.

  \section{Practice questions}

    \begin{exercise}
      Look at the material from Chapter \ref{chapter_introduction} and
      in particular the \ref{introduction_questions}.
      Write code to numerically test some idea you have.
      For example, can you use \pythoninline{matplotlib} to 
      plot Wilkinson's polynomial. What happens when you increase the
      number of integer roots beyond $20$?
    \end{exercise}

    \begin{exercise}
      Listing \ref{listing_python_BW_scipy_mandelbrot} gives example code
      of how to plot a black and white picutre of the mandelbrot set.
      It is possible to add color, take a look at
      \url{https://rosettacode.org/wiki/Mandelbrot_set#Python}
      or even 
      \url{https://www.codingame.com/playgrounds/2358/how-to-plot-the-mandelbrot-set/adding-some-colors}.
      You will need to read about the \pythoninline{imshow} function.
      The documentation is at
      \url{https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.pyplot.imshow.html}.
      \begin{listing}
        \caption{Python code to plot the Mandelbrot set. Take
          from \url{https://scipy-lectures.org/intro/numpy/auto_examples/plot_mandelbrot.html}.}
        \label{listing_python_BW_scipy_mandelbrot}
        \begin{python}
          import numpy as np
          import matplotlib.pyplot as plt
          from numpy import newaxis

          def compute_mandelbrot(N_max, some_threshold, nx, ny):
              # A grid of c-values
              x = np.linspace(-2, 1, nx)
              y = np.linspace(-1.5, 1.5, ny)

              c = x[:,newaxis] + 1j*y[newaxis,:]

              # Mandelbrot iteration

              z = c
              for j in range(N_max):
                  z = z**2 + c

              mandelbrot_set = (abs(z) < some_threshold)

              return mandelbrot_set

          mandelbrot_set = compute_mandelbrot(50, 50., 601, 401)

          plt.imshow(mandelbrot_set.T, extent=[-2, 1, -1.5, 1.5])
          plt.gray()
          plt.show()
        \end{python}
      \end{listing}
    \end{exercise}

    \begin{exercise}
      Go to 
      \url{https://adriann.github.io/programming_problems.html}
      and pick an example problem to work on, working in 
      either C or Python.
      You will most likely need to read the C language documentation
      \url{https://en.cppreference.com/w/c/language} to find out how to
      do what you need to do.
    \end{exercise}

    \begin{exercise}
      Python makes some things super easy, like getting data from api's.
      Listing \ref{listing_python_chuck_norris_joke} gives an example.
      The website \url{https://rapidapi.com/collection/cool-apis} lists
      some interesting server API's.
      This site, \url{https://www.dataquest.io/blog/python-api-tutorial/},
      has a nice overview of using the Python \pythoninline{requests}
      and \pythoninline{json} packages to query APIs.
      \begin{listing}
        \caption{Asks a server to return a random Chuck Norris joke.}
        \label{listing_python_chuck_norris_joke}
        \begin{python}
          import json
          import requests

          get_random_joke = "https://api.chucknorris.io/jokes/random"

          r = requests.get(get_random_joke)
          data = json.loads(r.text)
          print json.dumps(data, indent=4, sort_keys=True)
        \end{python}
      \end{listing}
    \end{exercise}

    \begin{exercise}
      Build a bot to interact with Reddit.
      See \url{https://www.pythonforengineers.com/build-a-reddit-bot-part-1/}.
    \end{exercise}
    
    
